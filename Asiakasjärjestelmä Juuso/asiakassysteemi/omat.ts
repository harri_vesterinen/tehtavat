// esim. tässä funktiot, joita halutaan testata 
// ks. tiedosto omat.ts, test_matikkafunktiot.ts, testiAjo.ts

export function laskeLuvutYhteen(x,y) {
    return x+y;
}
export function kerroLuvutKeskenään(x,y) {
    return x*y;
}