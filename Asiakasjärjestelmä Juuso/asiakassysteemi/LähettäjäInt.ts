export interface LähettäjäInt {

    lähetä(vastaanottaja: string, viesti: string): void;

}