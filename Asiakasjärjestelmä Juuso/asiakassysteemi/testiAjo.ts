// testiajon suorittava komponentti
// ks. tiedosto omat.ts, test_matikkafunktiot.ts, testiAjo.ts

import {t_laskeLuvutYhteen, t_kerroLuvutKeskenään} from './test_matikkafunktiot'

runTests()

function runTests() {
    t_laskeLuvutYhteen();
    t_kerroLuvutKeskenään();
}