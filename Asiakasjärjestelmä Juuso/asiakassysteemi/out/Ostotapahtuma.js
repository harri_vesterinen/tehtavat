"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Tila;
(function (Tila) {
    Tila[Tila["kesken"] = 0] = "kesken";
    Tila[Tila["luotu"] = 1] = "luotu";
    Tila[Tila["maksettu"] = 2] = "maksettu";
})(Tila = exports.Tila || (exports.Tila = {}));
var Ostotapahtuma = /** @class */ (function () {
    function Ostotapahtuma(asiakas, tuote, määrä) {
        this.tapahtumanTila = Tila.kesken;
        this.määrä = määrä;
        this.asiakas = asiakas;
        this.tuote = tuote;
        this.kuuntelijat = [];
    }
    Ostotapahtuma.prototype.lisääKuuntelija = function (kuuntelija) {
        // Onkohan siellä kuuntelijassa mitään? FIXME!
        this.kuuntelijat.push(kuuntelija);
    };
    Ostotapahtuma.prototype.poistaKuuntelija = function (kuuntelija) {
        // Onkohan siellä kuuntelijassa mitään? FIXME!
        //this.kuuntelijat.push(kuuntelija);
    };
    Ostotapahtuma.prototype.ilmianna = function () {
        var _this = this;
        this.kuuntelijat.forEach(function (kuuntelija) {
            kuuntelija.tapahtui(_this);
        });
    };
    Object.defineProperty(Ostotapahtuma.prototype, "$tuote", {
        /**
         * Getter $tuote
         * @return {Tuote}
         */
        get: function () {
            return this.tuote;
        },
        /**
         * Setter $tuote
         * @param {Tuote} value
         */
        set: function (value) {
            this.tuote = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Ostotapahtuma.prototype, "$ostohetki", {
        /**
         * Getter $ostohetki
         * @return {Date}
         */
        get: function () {
            return this.ostohetki;
        },
        /**
         * Setter $ostohetki
         * @param {Date} value
         */
        set: function (value) {
            this.ostohetki = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Ostotapahtuma.prototype, "$asiakas", {
        /**
         * Getter $asiakas
         * @return {Asiakas}
         */
        get: function () {
            return this.asiakas;
        },
        /**
         * Setter $asiakas
         * @param {Asiakas} value
         */
        set: function (value) {
            this.asiakas = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Ostotapahtuma.prototype, "$tapahtumanTila", {
        /**
         * Getter $tapahtumanTila
         * @return {Tila}
         */
        get: function () {
            return this.tapahtumanTila;
        },
        /**
         * Setter $tapahtumanTila
         * @param {Tila} value
         */
        set: function (value) {
            if (value != this.tapahtumanTila) {
                this.tapahtumanTila = value;
                this.ilmianna();
            }
        },
        enumerable: true,
        configurable: true
    });
    return Ostotapahtuma;
}());
exports.Ostotapahtuma = Ostotapahtuma;
//# sourceMappingURL=Ostotapahtuma.js.map