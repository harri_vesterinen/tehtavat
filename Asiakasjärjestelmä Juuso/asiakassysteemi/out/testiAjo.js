"use strict";
// voitaisiin laittaa omaan tiedostoon kaikki ajettavien testien kutsut
// ks. tiedosto omat.ts
Object.defineProperty(exports, "__esModule", { value: true });
var test_matikkafunktiot_1 = require("./test_matikkafunktiot");
runTests();
function runTests() {
    test_matikkafunktiot_1.t_laskeLuvutYhteen();
    test_matikkafunktiot_1.t_kerroLuvutKeskenään();
}
//# sourceMappingURL=testiAjo.js.map