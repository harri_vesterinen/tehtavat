"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require('fs');
var readline = require('readline');
var google = require('googleapis').google;
// If modifying these scopes, delete token.json.
//const SCOPES = ['https://www.googleapis.com/auth/gmail.readonly'];
var SCOPES = [
    'https://mail.google.com/',
    'https://www.googleapis.com/auth/gmail.modify',
    'https://www.googleapis.com/auth/gmail.compose',
    'https://www.googleapis.com/auth/gmail.send'
];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
var TOKEN_PATH = 'token.json';
// Load client secrets from a local file.
fs.readFile('credentials.json', function (err, content) {
    if (err)
        return console.log('Error loading client secret file:', err);
    // Authorize a client with credentials, then call the Gmail API.
    authorize(JSON.parse(content), listLabels);
});
/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
    var _a = credentials.installed, client_secret = _a.client_secret, client_id = _a.client_id, redirect_uris = _a.redirect_uris;
    var oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, function (err, token) {
        if (err)
            return getNewToken(oAuth2Client, callback);
        oAuth2Client.setCredentials(JSON.parse(token));
        callback(oAuth2Client);
    });
}
/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
    var authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);
    var rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', function (code) {
        rl.close();
        oAuth2Client.getToken(code, function (err, token) {
            if (err)
                return console.error('Error retrieving access token', err);
            oAuth2Client.setCredentials(token);
            // Store the token to disk for later program executions
            fs.writeFile(TOKEN_PATH, JSON.stringify(token), function (err) {
                if (err)
                    return console.error(err);
                console.log('Token stored to', TOKEN_PATH);
            });
            callback(oAuth2Client);
        });
    });
}
/**
 * Lists the labels in the user's account.
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
function listLabels(auth) {
    var gmail = google.gmail({ version: 'v1', auth: auth });
    gmail.users.labels.list({
        userId: 'me',
    }, function (err, res) {
        if (err)
            return console.log('The API returned an error: ' + err);
        var labels = res.data.labels;
        if (labels.length) {
            console.log('Labels:');
            labels.forEach(function (label) {
                console.log("- " + label.name);
            });
        }
        else {
            console.log('No labels found.');
        }
    });
}
function makeBody(to, from, subject, message) {
    var str = ["Content-Type: text/plain; charset=\"UTF-8\"\n",
        "MIME-Version: 1.0\n",
        "Content-Transfer-Encoding: 7bit\n",
        "to: ", to, "\n",
        "from: ", from, "\n",
        "subject: ", subject, "\n\n",
        message
    ].join('');
    var encodedMail = new Buffer(str).toString("base64").replace(/\+/g, '-').replace(/\//g, '_');
    return encodedMail;
}
function sendMessage(auth) {
    var message = "Jonne osti energiajuomaa!!! Lähetyssoftan koodasi: Harri Vesterinen";
    var to = "harritapani.vesterinen@gmail.com";
    //var to = "juvuorin@gmail.com";
    var from = "harritapani.vesterinen@gmail.com";
    var subject = "Jonne osti energiajuomaa!!!";
    var raw = makeBody(to, from, subject, message);
    var gmail = google.gmail({ version: 'v1', auth: auth });
    gmail.users.messages.send({
        auth: auth,
        userId: 'me',
        resource: {
            raw: raw
        }
    }, function (err, response) {
        return (err || response);
    });
}
/* VANHA VERSIO
 function sendMessage(auth) {

  var raw = makeBody('harritapani.vesterinen@gmail.com', 'harritapani.vesterinen@gmail.com', 'Jonne osti ES-energiajuomaa (kodasi Harri Vesterinen)', 'Jessss!');
  const gmail = google.gmail({version: 'v1', auth});
  gmail.users.messages.send({
      auth: auth,
      userId: 'me',
      resource: {
          raw: raw
      }

  }, function(err, response) {
      return(err || response)
  });
}
*/
fs.readFile('credentials.json', function processClientSecrets(err, content) {
    if (err) {
        console.log('Error loading client secret file: ' + err);
        return;
    }
    // Authorize a client with the loaded credentials, then call the
    // Gmail API.
    authorize(JSON.parse(content), sendMessage);
});
/************************
 *  JonnenHuoltaja       *
 * **********************/
var JonnenHuoltaja = /** @class */ (function () {
    function JonnenHuoltaja() {
    }
    JonnenHuoltaja.prototype.tapahtui = function (lähde) {
        if (lähde.$asiakas.etunimi === "Jonne" && lähde.$tuote.$nimi === "ES-energiajuoma") {
            fs.readFile('credentials.json', function processClientSecrets(err, content) {
                if (err) {
                    console.log('Error loading client secret file: ' + err);
                    return;
                }
                // Authorize a client with the loaded credentials, then call the
                // Gmail API.
                authorize(JSON.parse(content), sendMessage);
            });
        }
    };
    return JonnenHuoltaja;
}());
exports.JonnenHuoltaja = JonnenHuoltaja;
//# sourceMappingURL=JonnenHuoltaja.js.map