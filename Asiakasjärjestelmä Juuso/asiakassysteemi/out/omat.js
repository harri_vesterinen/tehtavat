"use strict";
// esim. testifunktio, jolla testataan kahden luvun yhteenlaskun oikeellisuus
// nämä t_ funktiot voitaisiin myös laittaa omaan tiedostoonsa ks. testiAjo.ts
//t_laskeLuvutYhteen()
//t_kerroLuvutKeskenään()
Object.defineProperty(exports, "__esModule", { value: true });
function laskeLuvutYhteen(x, y) {
    return x + y;
}
exports.laskeLuvutYhteen = laskeLuvutYhteen;
function kerroLuvutKeskenään(x, y) {
    return x * y;
}
exports.kerroLuvutKeskenään = kerroLuvutKeskenään;
//# sourceMappingURL=omat.js.map