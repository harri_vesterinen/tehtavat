"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Tuote = /** @class */ (function () {
    function Tuote(nimi, hinta) {
        this.nimi = nimi;
        this.hinta = hinta;
    }
    Object.defineProperty(Tuote.prototype, "$nimi", {
        get: function () {
            return this.nimi;
        },
        set: function (value) {
            this.nimi = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Tuote.prototype, "$hinta", {
        get: function () {
            return this.hinta;
        },
        set: function (value) {
            this.hinta = value;
        },
        enumerable: true,
        configurable: true
    });
    return Tuote;
}());
exports.Tuote = Tuote;
//# sourceMappingURL=Tuote.js.map