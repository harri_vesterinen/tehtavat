"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*******
 * HVe: asennettu seuraavat node.js paketit:
 *  npm i @types/node
 *  npm install --save js-base64
 */
var js_base64_1 = require("js-base64"); // HVe: https://www.npmjs.com/package/js-base64
/************************
 *  JonnenHuoltaja       *
 * **********************/
var JonnenHuoltaja = /** @class */ (function () {
    function JonnenHuoltaja() {
    }
    JonnenHuoltaja.prototype.tapahtui = function (lähde) {
        console.log("Piiskaa, taas Jonne menit ostamaan jotain!");
        // HVE
        if (lähde.$asiakas.etunimi === "Jonne" && lähde.$tuote.$nimi === "ES-energiajuoma") {
            sendMessage(userId, email, callbackFunktio);
        }
    };
    return JonnenHuoltaja;
}());
exports.JonnenHuoltaja = JonnenHuoltaja;
/** Harrin lisäykset alkaa tästä**************** */
var google = require('googleapis').google;
var gmail = google.gmail('v1');
var callbackFunktio = function viestiOK() { console.log("Viesti lähetetty ok!"); };
//let userId = 'me';
var userId = 'harritapani.vesterinen@gmail.com';
var email = "Jonne on ostanut ES-energiajuomaa!!! (lähetyssoftan koodasi Harri Vesterinen).";
/**
 * Send Message.
 *
 * @param  {String} userId User's email address. The special value 'me'
 * can be used to indicate the authenticated user.
 * @param  {String} email RFC 5322 formatted String.
 * @param  {Function} callback Function to call when the request is complete.
 */
function sendMessage(userId, email, callbackFunktio) {
    // Using the js-base64 library for encoding:
    // https://www.npmjs.com/package/js-base64
    var base64EncodedEmail = js_base64_1.Base64.encodeURI(email);
    var request = gmail.users.messages.send({
        //let request = google.client.gmail.users.messages.send({
        'userId': userId,
        'resource': {
            'raw': base64EncodedEmail,
            "payload": {
                "headers": [
                    {
                        "name": "To",
                        "value": "harritapani.vesterinen@gmail.com"
                    }
                ]
            }
        }
    });
    //request.execute(callbackFunktio);
    console.log("Kukkuu");
}
//# sourceMappingURL=JonnenHuoltaja_vanha.js.map