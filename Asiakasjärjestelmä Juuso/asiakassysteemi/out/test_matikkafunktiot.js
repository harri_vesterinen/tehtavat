"use strict";
// esim. testifunktio, jolla testataan kahden luvun yhteenlaskun oikeellisuus
// nämä t_ funktiot voitaisiin myös laittaa omaan tiedostoonsa ks. testiAjo.ts
//t_laskeLuvutYhteen()
//t_kerroLuvutKeskenään()
Object.defineProperty(exports, "__esModule", { value: true });
var omat_1 = require("./omat");
function t_laskeLuvutYhteen() {
    // mietittävä, kuinka kattavasti on järkevää testata
    // aina ei voi testata kaikkia tapauksia
    if (omat_1.laskeLuvutYhteen(2, 3) == 5) {
        console.log("Testi 1. meni läpi.");
    }
    else {
        console.log("Testi 1. ei mennyt läpi.");
    }
    if (omat_1.laskeLuvutYhteen(7, 9) == 16) {
        console.log("Testi 2. meni läpi.");
    }
    else {
        console.log("Testi 2. ei mennyt läpi.");
    }
}
exports.t_laskeLuvutYhteen = t_laskeLuvutYhteen;
function t_kerroLuvutKeskenään() {
    if (omat_1.kerroLuvutKeskenään(5, 6) === 30) {
        console.log("Testi 3. meni läpi");
    }
    else {
        console.log("Testi 3. ei mennyt läpi");
    }
}
exports.t_kerroLuvutKeskenään = t_kerroLuvutKeskenään;
//# sourceMappingURL=test_matikkafunktiot.js.map