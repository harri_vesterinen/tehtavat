import {PerusKantaAsiakas} from './PerusKantaAsiakas'
import { Tuote } from './Tuote'
import { JonnenHuoltaja } from './JonnenHuoltaja_vanha'
import { Ostotapahtuma, Tila } from './Ostotapahtuma'

let asiakas = new PerusKantaAsiakas("Jonne","Välimaa")
let tuote = new Tuote("ES-energiajuoma",5)
let jonnenhuoltaja = new JonnenHuoltaja();
let ostos = new Ostotapahtuma(asiakas, tuote, 4);
ostos.lisääKuuntelija(jonnenhuoltaja);

ostos.$tapahtumanTila = Tila.maksettu;

