// testifunktiot
// t_ funktiot 
// ks. tiedosto omat.ts, test_matikkafunktiot.ts, testiAjo.ts

import {laskeLuvutYhteen, kerroLuvutKeskenään} from './omat'

export function t_laskeLuvutYhteen() {

    // mietittävä, kuinka kattavasti on järkevää testata
    // aina ei voi testata kaikkia tapauksia
    if (laskeLuvutYhteen(2,3)==5) {
        console.log("Testi 1. meni läpi.")
    } else {
        console.log("Testi 1. ei mennyt läpi.")
    }

    if (laskeLuvutYhteen(7,9)==16) {
        console.log("Testi 2. meni läpi.")
    } else {
        console.log("Testi 2. ei mennyt läpi.")
    }

}

export function t_kerroLuvutKeskenään() {
    if (kerroLuvutKeskenään(5,6)===30) {
        console.log("Testi 3. meni läpi")
    } else {
        console.log("Testi 3. ei mennyt läpi")
    }
}
