import { OstotapahtumaKuuntelijaInt } from "./OstotapahtumaKuuntelijaInt";
import {Ostotapahtuma } from "./Ostotapahtuma";

/*******
 * HVe: asennettu seuraavat node.js paketit:
 *  npm i @types/node
 *  npm install --save js-base64
 */
import { Base64 } from 'js-base64'; // HVe: https://www.npmjs.com/package/js-base64


/************************
 *  JonnenHuoltaja       *
 * **********************/
export class JonnenHuoltaja implements OstotapahtumaKuuntelijaInt {
    tapahtui(lähde: Ostotapahtuma): void {
      console.log("Piiskaa, taas Jonne menit ostamaan jotain!");

      // HVE
      if (lähde.$asiakas.etunimi === "Jonne" && lähde.$tuote.$nimi === "ES-energiajuoma") {
        sendMessage(userId, email, callbackFunktio);
      }

    }
}

/** Harrin lisäykset alkaa tästä**************** */
const {google} = require('googleapis');
const gmail = google.gmail('v1');

const callbackFunktio = function viestiOK() {console.log("Viesti lähetetty ok!")};
//let userId = 'me';
let userId = 'harritapani.vesterinen@gmail.com';
let email = "Jonne on ostanut ES-energiajuomaa!!! (lähetyssoftan koodasi Harri Vesterinen).";

/**
 * Send Message.
 *
 * @param  {String} userId User's email address. The special value 'me'
 * can be used to indicate the authenticated user.
 * @param  {String} email RFC 5322 formatted String.
 * @param  {Function} callback Function to call when the request is complete.
 */

function sendMessage(userId, email, callbackFunktio) {
    // Using the js-base64 library for encoding:
    // https://www.npmjs.com/package/js-base64
    let base64EncodedEmail = Base64.encodeURI(email);
    let request = gmail.users.messages.send({
    //let request = google.client.gmail.users.messages.send({
      'userId': userId,
      'resource': {
        'raw': base64EncodedEmail,
        "payload": {
          "headers": [
            {
              "name": "To",
              "value": "harritapani.vesterinen@gmail.com"
            }
          ]
        }
      
      }
    });
    //request.execute(callbackFunktio);
    console.log("Kukkuu");
  }
  