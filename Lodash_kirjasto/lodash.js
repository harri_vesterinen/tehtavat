// ks. https://lodash.com/
// asenna: 
/* 
$ npm i -g npm
$ npm i --save lodash
*/

const _ = require("lodash")

let taulukko = [{ "ma": 44 }, { "pe": 100 }, { "ke": 21 }, { "ti": 66 }, { "la": 22 }]

let randomPick = _.sample(taulukko); // sample -> random elementti

console.log("satunnainen pala:")
console.log(randomPick)

console.log("taulukko:")
console.log(taulukko)
console.log("sekoitettu taulukko:")

console.log(_.shuffle(taulukko));  // sekoitus satunnaiseen järjestykseen


//////////////////////////////////////////////////

let paivat = ["ma", "ti", "ke", "to", "pe", "la", "su"];

console.log("alkaa kirjaimella t: ")

paivat.forEach(e => {

    if (_.startsWith(e, 't')) {

        console.log(e);
    }
});

console.log("loppuu kirajimeen a")

paivat.forEach(e => {

    if (_.endsWith(e, 'a')) {

        console.log(e);
    }
});

