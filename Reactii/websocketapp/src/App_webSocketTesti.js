import React, { useState, useEffect } from 'react';
import './App.css';

function App() {

  const [websocket, setWebsocket] = useState(new WebSocket('wss://echo.websocket.org/'));
  const [viesti, setViesti] = useState("");
  const [yhteys, setYhteys] = useState("Ei ole yhteydessä");

  function suljeYhteys() {
    websocket.close();
  }

  function avaaYhteys() {
    setWebsocket(new WebSocket('wss://echo.websocket.org/'))
  }

  function sendMessage() {
    try {
      websocket.send("Toimiikohan?");
      console.log("Viesti lähtee!")
    }
    catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    websocket.onopen = () => {
      console.log("Yhteys luotu!");
      setYhteys("Yhteys päällä");
    };

    websocket.onclose = () => {
      console.log('disconnected')
      setYhteys("Ei ole yhteydessä")
    };

    websocket.onmessage = evt => {
      // listen to data sent from the websocket server
      setViesti(evt.data)
      console.log(evt.data)
    }

  })

  return (
    <div className="App">
      <p>Yhteyden tila: {yhteys}</p>
      <input type="button" onClick={suljeYhteys} value="Sulje yhteys" /><input type="button" onClick={avaaYhteys} value="Avaa yhteys" />
      <p>Viesti: {viesti}</p>
      <input type="button" onClick={sendMessage} value="Lähetä viesti" />

    </div>
  );
}

export default App;