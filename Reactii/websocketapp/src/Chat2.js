import React, { useState, useEffect } from 'react';
import './Chat2.css'

/*
    "Featuret" : - Samaa viestiä ei oteta vastaan muuta kuin kerran...
*/


function Chat2() {
    const [websocket, setWebsocket] = useState(null);
    const [viesti, setViesti] = useState("");
    const [historia, setHistoria] = useState(new Array(50).fill(''));
    const [url, setUrl] = useState("wss://echo.websocket.org");
    const [saatuViesti, setSaatuViesti] = useState("");
    const [tila, setTila] = useState(3);

    const omaViestiTyyli = {
        fontSize: "30px",
        textAlign: "left",
        backgroundColor: "#FF1493",
        transform: "rotate(180deg)"
    };
    const randomViestiTyyli = {
        fontSize: "18px",
        textAlign: "right",
        backgroundColor: "#FFD700",
        transform: "rotate(180deg)"
    };

    // // Muistutuksena
    // const tilat = {
    //     0: "Yhdistää...",
    //     1: "Yhdistetty",
    //     2: "Sulkeutuu...",
    //     3: "Suljettu"
    //   };

    function avaaYhteys() {
        if (websocket !== null) {
            alert("Sulje ensin vanha yhteys!");
        }
        else {
            const uusiUrl = prompt("Anna url", "");
            setUrl(uusiUrl);
        }
    }

    function suljeYhteys() {
        if (websocket !== null) {
            setTimeout(() => setTila(websocket.readyState), 500)
            websocket.close();
            setUrl("");
        }
    }

    function lahetaViesti(viesti) {
        const uusiHistoria = Array.from(historia);
        if (websocket !== null && tila === 1) {
            websocket.send(viesti);
            uusiHistoria.unshift("> " + viesti);
        }
        else {
            uusiHistoria.unshift("> DISCONNECTED");
        }
        uusiHistoria.pop();
        setHistoria(uusiHistoria);
        setViesti("");
    }

    function muutaHistoriaa() {
        if (tila === 1) {
            console.log("historia muuttuu")
            const uusiHistoria = Array.from(historia);
            uusiHistoria.unshift(saatuViesti + " <");
            uusiHistoria.pop();
            setHistoria(uusiHistoria);

        }
    }

    useEffect(() => {

        console.log("URL muuttuuu");

        if (url !== "") {

            try {
                const ws = new WebSocket(url);
                setTimeout(() => setTila(ws.readyState), 500)

                ws.onopen = () => {
                    console.log("Yhteys luotu!");
                    setTila(1);
                };
                ws.onclose = () => {
                    console.log("Yhteys kiinni")
                    ws.close();
                    setWebsocket(null)
                    setTila(3);
                };
                ws.onmessage = evt => {
                    setSaatuViesti(evt.data);
                }
                ws.onerror = evt => {
                    console.log("Virhe: " + evt)
                    setTila(3);
                }

                setWebsocket(ws);
            }
            catch (error) {
                alert("Virheellinen Websocket\n\nkatso lisätiedot konsolista")
                console.log(error);
            }
        }
    }, [url])

    useEffect(muutaHistoriaa, [saatuViesti])

    return (
        <div className="Chat2">
            {tila === 1 && <h2 id="paalla">Yhteys päällä</h2>}{tila === 3 && <h2 id="pois">Yhteys suljettu</h2>}
            {tila === 2 && <h2>Suljetaan...</h2>}{tila === 0 && <h2>Yhdistetään...</h2>}
            <input type="button" onClick={avaaYhteys} value="Avaa yhteys" />
            <input type="button" onClick={suljeYhteys} value="Sulje yhteys" />
            <h1>Chattiohjelma</h1>
            <input type="text" value={viesti} onChange={(e) => setViesti(e.target.value)}
                onKeyDown={(e) => e.keyCode === 13 ? lahetaViesti(viesti) : setViesti(viesti)} />
            <input type="button" onClick={() => lahetaViesti(viesti)} value="Lähetä viesti" />
            <form>
                <fieldset id="fieldi">
                    {historia.map(arvo =>
                        arvo[0] === '>' ? <p style={omaViestiTyyli}>{arvo}</p> : <p style={randomViestiTyyli}>{arvo}</p>
                    )}</fieldset>
            </form>
        </div>
    );
}

export default Chat2;