import React, { useState, useEffect } from 'react';
import './App.css';

function App() {

  const [websocket, setWebsocket] = useState(new WebSocket('wss://connect.websocket.in/v2/150?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjExNTcyNDMyNWQzODkyMzMxN2Q3YTQ1MmMxYzQ4NjQ2OWE0NmE1NTZiYzkwNzY2ZTc0YTk1MDMwZDQ5MGVlYjczYzBkOTQ1OGY3YzIyMzQwIn0.eyJhdWQiOiI2IiwianRpIjoiMTE1NzI0MzI1ZDM4OTIzMzE3ZDdhNDUyYzFjNDg2NDY5YTQ2YTU1NmJjOTA3NjZlNzRhOTUwMzBkNDkwZWViNzNjMGQ5NDU4ZjdjMjIzNDAiLCJpYXQiOjE1NzQ2ODE2NDAsIm5iZiI6MTU3NDY4MTY0MCwiZXhwIjoxNjA2MzA0MDQwLCJzdWIiOiI2NiIsInNjb3BlcyI6W119.nv65lQYpq492Cbc1blQkX3b8nu_gruEKkVLRdjsDrynhZ3unDJwanToxD6F6l3lQFrxhRSEzczHeJrcoC6cuEsMbP3F1tL88DfwbDPBWWSLRr7dqgBW2do5VU6T-r106-4OEcNMK1dObboPsT2GznZEx43UF-ZBVXLf4_iY8JxG8J5wydolXNxr2ICrmG6i4QTEcd1C5WjoTKXYMYOZTh2YUwyu8ETGuN7jTUsQNz3SJ8am2WYKpD3ZHuzBCk-eKaGD6IP887vvMOBhhpY51ccukfpcXS8NBN_-pW3lAymD2apPpxuvARcU57Je_UUZRoJdUOaX7EqGOvahI2coqjLdgx6irlqNfXx7JfMq34FffJ851u4gAYOWYaX6pDZTpmEL7zq1JIuPs0iMR6N6UpHfWX1X9tNetrZN1mQpVEPwqwfc6hDIxaM3H-B4ebT_1JYkC_NWr2aD9amRJJIqR3l91GTO255Qq5OXH3J1x-8D8ZYBsDLnYESKbL3kO_5Sa0a9vwF_JYdlHthDg2VrY2hvS56YrM0xyqE5ZY4bBaXvXsMr66GdykTP8GFzWecG0iWPkiYY_NSYJY7-zp6TjklH076AWGUd-CS3-ho0FXgnPNnCng02ptzaGqrVA_hFqotzHo-HfIBNFyhc3jNu7H8ByVrt1dNtxJgGAJWmFnhk'));
  const [viesti, setViesti] = useState("");
  const [yhteys, setYhteys] = useState(true);
  const [historia, setHistoria] = useState(new Array(9).fill(''));

  function suljeYhteys() {
    websocket.close();
  }

  function avaaYhteys() {
    setWebsocket(new WebSocket(' wss://connect.websocket.in/v2/150?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjExNTcyNDMyNWQzODkyMzMxN2Q3YTQ1MmMxYzQ4NjQ2OWE0NmE1NTZiYzkwNzY2ZTc0YTk1MDMwZDQ5MGVlYjczYzBkOTQ1OGY3YzIyMzQwIn0.eyJhdWQiOiI2IiwianRpIjoiMTE1NzI0MzI1ZDM4OTIzMzE3ZDdhNDUyYzFjNDg2NDY5YTQ2YTU1NmJjOTA3NjZlNzRhOTUwMzBkNDkwZWViNzNjMGQ5NDU4ZjdjMjIzNDAiLCJpYXQiOjE1NzQ2ODE2NDAsIm5iZiI6MTU3NDY4MTY0MCwiZXhwIjoxNjA2MzA0MDQwLCJzdWIiOiI2NiIsInNjb3BlcyI6W119.nv65lQYpq492Cbc1blQkX3b8nu_gruEKkVLRdjsDrynhZ3unDJwanToxD6F6l3lQFrxhRSEzczHeJrcoC6cuEsMbP3F1tL88DfwbDPBWWSLRr7dqgBW2do5VU6T-r106-4OEcNMK1dObboPsT2GznZEx43UF-ZBVXLf4_iY8JxG8J5wydolXNxr2ICrmG6i4QTEcd1C5WjoTKXYMYOZTh2YUwyu8ETGuN7jTUsQNz3SJ8am2WYKpD3ZHuzBCk-eKaGD6IP887vvMOBhhpY51ccukfpcXS8NBN_-pW3lAymD2apPpxuvARcU57Je_UUZRoJdUOaX7EqGOvahI2coqjLdgx6irlqNfXx7JfMq34FffJ851u4gAYOWYaX6pDZTpmEL7zq1JIuPs0iMR6N6UpHfWX1X9tNetrZN1mQpVEPwqwfc6hDIxaM3H-B4ebT_1JYkC_NWr2aD9amRJJIqR3l91GTO255Qq5OXH3J1x-8D8ZYBsDLnYESKbL3kO_5Sa0a9vwF_JYdlHthDg2VrY2hvS56YrM0xyqE5ZY4bBaXvXsMr66GdykTP8GFzWecG0iWPkiYY_NSYJY7-zp6TjklH076AWGUd-CS3-ho0FXgnPNnCng02ptzaGqrVA_hFqotzHo-HfIBNFyhc3jNu7H8ByVrt1dNtxJgGAJWmFnhk'));
  }

  function sendMessage(msg) {
    try {
      const uusiHistoria = Array.from(historia);
      if (yhteys) {
        uusiHistoria.push("> " + msg);
        websocket.send(msg);
      }
      else {
        uusiHistoria.push("> DISCONNECTED")
      }
      uusiHistoria.shift();
      setHistoria(uusiHistoria);
      setViesti("");
    }
    catch (error) {
      console.log(error);
    }
  }




  useEffect(() => {
    websocket.onopen = () => {
      console.log("Yhteys luotu!");
      setYhteys(true);
    };

    websocket.onclose = () => {
      console.log("Yhteys kiinni")
      setYhteys(false)
    };

    websocket.onmessage = evt => {
      // listen to data sent from the websocket server
      const uusiHistoria = Array.from(historia);
      uusiHistoria.push(evt.data + "<");
      uusiHistoria.shift();
      setHistoria(uusiHistoria);
    }

    websocket.onerror = evt => {
      console.log("HAIRio"+ evt)
    }

  })

  return (
    <div className="App">
      {yhteys ? <h1>Yhteys päällä</h1> : <h1>Yhteys pois päältä</h1>}
      <input type="button" onClick={suljeYhteys} value="Sulje yhteys" />
      <input type="button" onClick={avaaYhteys} value="Avaa yhteys" />
      <p>Lähetetään viestejä</p>
      <input type="text" value={viesti} onChange={(e) => setViesti(e.target.value)} onKeyDown={(e) => e.keyCode === 13 ? sendMessage(viesti) : setViesti(viesti)} />
      <input type="button" onClick={() => sendMessage(viesti)} value="Lähetä viesti" />
      <p>CHAT:</p>
      <form>
        <fieldset>
          {historia.map(arvo =>
            arvo[0] === '>' ? <p style={omaViestiTyyli}>{arvo}</p> : <p style={randomViestiTyyli}>{arvo}</p>
          )}</fieldset>
      </form>
    </div>
  );
}
const omaViestiTyyli = {
  fontSize: "30px",
  textAlign: "left",
  backgroundColor: "#FF1493"
};
const randomViestiTyyli = {
  fontSize: "18px",
  textAlign: "right",
  backgroundColor: "#FFD700"
};

export default App;