import React from 'react';
import logo from './logo.svg';
import './App.css';

function Lapsi() {
  return (
    <div className="Lapsi">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          <code>Tämä on Lapsi-komponentti</code>
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default Lapsi;
