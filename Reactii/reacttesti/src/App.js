import React, { useState } from 'react'; // tuodaan myös useState
import Ruutu from './Ruutu'
import './App.css';
import './tictactoe.css';

// 3 * 3 ruudun ristinollapeli.
// Toteutus ilman reactin luokkakomponentteja hooksien avulla (useState-huukki)

function App() {

  const [lauta, setLauta] = useState(["","","","","","","","",""]);
  const [onkoXSeuraava, setOnkoXSeuraava ] = useState(true);
  const seuraavaMerkki = onkoXSeuraava ? "X" : "O";
  const voittaja = laskeVoittaja(lauta);

  // piirrä pelilaudan ruutu:
  function piirraRuutu(i) {
    return (
      <Ruutu
        key={i} // key-arvo, jonka React vaatii listoissa. (yleensä ei tosin käytetä indexiä, koska
        // jos listan väliin lisätään tai sieltä poistetaan arvoja, ei hyvä!)
        pelimerkki={lauta[i]}
        RuutuOnClick={() => {
            if (lauta[i] !== "" || voittaja != null) {
              return;
            }
            /* JavaScriptissä muuttuja = taulukko[] on VIITTAUS taulukkoon (tai mihin tahansa OBJEKTIIN )
              Näin ollen 'muuttuja' on VIITTAUS. taulukko[] (tai objektin) sisältöä VOI muuttaa edelleen,
              mutta 'muuttuja'-viittausta EI voi muuttaa!
              Näin ollen alla oleva:
              const seuraavaLauta = lauta.slice()
              -> seuraavaLauta on viittaus uuteen taulukkoon -> VIITTAUSTA ei voi muuttaa
              -> taulukon sisältöä voi edelleen muuttaa.
            */
            const seuraavaLauta = lauta.slice(); // seuraavaLauta = [...lauta] <--- tätäkin voisi käyttää kopioidessa
            seuraavaLauta[i] = seuraavaMerkki;
            setLauta(seuraavaLauta);
            setOnkoXSeuraava(!onkoXSeuraava); // vaihda vuoroja
          }
        }
      />
    );
  }

  // tutki löytyykö voittoriviä
  function laskeVoittaja(lauta) {
    const voittoRivit = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];
    // käy läpi mahdolliset voittorivit ja tarkista, onko niissä pelkkiä X:iä tai O:ia
    for (let i = 0; i < voittoRivit.length; i++) {
      const [a, b, c] = voittoRivit[i];
      /* alla oltava ensin myös tarkistus: if (lauta[a] && ...  koska
          jos lauta on tyhjä, lauta[a] palauttaa 'false' eikä sen jälkeisiä
          ehtoja suoriteta. On sama kuin käyttäisi: lauta[a]!="".
          Muuten siis tyhjä lauta palauttaisi jo voittajan. 
      */
      if (lauta[a] && lauta[a] === lauta[b] && lauta[a] === lauta[c]) {
        return lauta[a];
      }
    }
    return null;
  }

  // Seuraa pelin statusta: onko voittajaa, onko lauta täynnä?
  function getStatus() {
    if (voittaja) {
      return "Voittaja: " + voittaja
    } else if (isBoardFull(lauta)) {
      return "Tasapeli!";
    } else {
      return "Seuraava pelaaja: " + seuraavaMerkki;
    }
  }

  // Pelaa uudestaan -nappi
  function renderRestartButton() {
    return (
      <Restart
        uusiPelionClick={() => {
          setLauta(Array(9).fill(""));
          setOnkoXSeuraava(true);
        }}
      />
    );
  }

  function Restart(props) {
    return (
      <button className="restart" onClick={props.uusiPelionClick}>
        Uusi peli!
      </button>
    );
  }

  // onko pelilauta täynnä?
  function isBoardFull(lauta) {
    for (let i = 0; i < lauta.length; i++) {
      if (lauta[i] === "") {
        return false;
      }
    }
    return true;
  }


  // ***** function App() return-osa alkaa *****
  return (   
    <div className="App">

    {voittaja!==null && <p>Onneksi olkoon!</p>}
    {voittaja==null &&
      <div>
        {lauta.map( (currentValue, i) => {
          return piirraRuutu(i)
        })}
      </div>
    }
      <div className="game-info">{getStatus()}</div>
      <div className="restart-button">{renderRestartButton()}</div>

    </div>
  );
}

export default App;
