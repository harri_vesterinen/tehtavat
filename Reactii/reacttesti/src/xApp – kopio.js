import React, { useState } from 'react'; // tuodaan myös useState
import logo from './logo.svg';
import './App.css';
import Lapsi from './Lapsi'

function App() {

  // useStaten käyttö tilamuuttujana
  const [teksti, setTeksti] = useState("Moikka!"); // kokeillaan teksti-muuttujaa ja sen settiä useStatessa
  const [laskuri, setLaskuri] = useState(0); // kokeillaan laskuri-muuttujaa ja sen settiä useStatessa
  // tänne voi kirjoittaa tavallista JavaScriptiä, eli ennen return() -kutsua

  // ks. https://reactjs.org/docs/events.html
  function omaFunktio(event) {
    // kutsutaan setTeksti / useStatea
    // viedään sille eventin targetin arvo
    setTeksti(event.target.value)
    console.log(event.target.value)
  }

  function omaFunktio2() {
    // lisätään myös laskuria kun hiiri menee tekstikentän päälle
    setLaskuri(laskuri + 1)
    console.log("Laskurin arvo:" + laskuri)
  }

  // return alkaa
  return (   
    <div className="App">
      {/* NÄIN JSX:n sekaan kommentteja eli return() sisällä on JSX:ää... */}
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Tää on App.js -komponentti......
        </p>       
      </header>
      {/* kutsutaan omaFunktio() -funkkaria aaltosulkeiden sisällä ilman sulkeita -> 
          on yhteys HTML ja JavaScript-koodin välillä.
          Tässä voisi käyttää onInput tai onChange eventtiä. Testataan myös onMouseOver:ia*/}
      <input type="text" id="name" value={teksti}
        onChange={omaFunktio} onMouseOver={omaFunktio2}/> 
      
      <button onClick={() => setTeksti("Terve!!!")}> {/* napilla useState-muuttujan päivittäminen */}
        {teksti}
      </button>

      <div>
        <Lapsi /> {/* tässä käytetään Lapsi.js -komponenttia */}
      </div>
    </div>
  );
}

export default App;
