import React from 'react';
import './App.css';
import './tictactoe.css';

function Ruutu(props) {

  // props.pelimerkki = ruudussa oleva merkki
  // props.RuutuOnClick = onClick event handler

  return (
  <div className="Ruutu">
    <button onClick={props.RuutuOnClick}>{props.pelimerkki}</button> 
  </div>
  ); 
}

export default Ruutu;
