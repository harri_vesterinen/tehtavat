import React,{ useState } from 'react';
import logo from './logo.svg';
import './App.css';
import Lapsi from './Lapsi'
import KayttoLiittyma17 from './KayttoLiittyma17'

// HUOM: tämä funktion kutsu vastaa React-komponentin Render():iä
// Itse html-piirto tapahtuu return(...) sisällä.
function App() {
  const [teksti, setTeksti] = useState("moi!");
  const [laskuri, setLaskuri] = useState(0);
  //const [obj, setObj] = useState({}); 

  function omaFunktio(event) {
    setTeksti(event.target.value) 
    console.log(event.target.value)
  }
  function omaKakkonen() {
    setLaskuri(laskuri+1)
    setLaskuri(laskuri+2)
    setLaskuri(laskuri+3)   // vain tämä rivi suorittuu tilamuuttujan muutokseen
    console.log(laskuri)
    
    //setLaskuri(laskuri+1)
    //setLaskuri(laskuri+1)
    //setLaskuri(laskuri+1)
    //console.log(laskuri)

    //setLaskuri(laskuri+1)
    //console.log(laskuri)
    // HVe:
    console.log("Tsekkaa laskurin arvo.")
  }
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {/*Tämä on kommentti */}
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >  
        </a>
      </header>

      {/* Tehtävä 1.7.A ja B: KayttoLiittyma17 */}
      <KayttoLiittyma17 />

      <p>*********</p>
      {/* oppitunnilla tehtyä alla */}
      <input type="text" id="name" value={teksti}
        onChange={omaFunktio} onMouseOver={omaKakkonen}/>        
      <button onClick={() => setTeksti("Terve!")}>
        {teksti}{laskuri}
      </button>
      <div>
        <Lapsi />
      </div>
    </div>
  );
}

export default App;