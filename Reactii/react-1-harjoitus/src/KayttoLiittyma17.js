import React, { useState } from 'react';
//import logo from './logo.svg';
import './App.css';

/*
TEHTÄVÄ 1.7.A
Tee käyttöliittymä, missä on kaksi tekstikenttää.
Kun ylempään tekstikenttään on kirjoitettu sana "teksti",
alempaan kenttään tulostuu sana "tekstiteksti", vain jos alempi kenttä on tyhjä.
Käytä esim. onChange-funktiota.

TEHTÄVÄ 1.7.B
Tee käyttöliittymä, missä on kaksi tekstikenttää.
Jos ylempi tekstikenttä menettää fokuksen (onBlur- tapahtuma),
alempaan kenttään tulostuu ylemmässä kentässä olevan sana kaksi kertaa peräkkäin,
mutta vain, jos alempi tekstikenttä on tyhjä ja ylemmässä tekstikentässä on tekstiä.   

Keksi 2 käytännön sovellusta, missä em. tilanteita voisi hyödyntää.
*/

// HUOM: tämä funktion kutsu vastaa React-komponentin Render():iä
// Itse html-piirto tapahtuu return(...) sisällä.
function KayttoLiittyma17() {

    // tilamuuttujat
    const [teksti1, setTeksti1] = useState("");
    const [teksti2, setTeksti2] = useState("");

    // TEHTÄVÄ 1.7.A: aseta kenttään "kenttä1" arvo: onChange-eventissä
    function asetaKentta1_onChange(event) {
        setTeksti1(event.target.value)
        console.log("onChange: " + teksti1)
        asetaKentta2_onChange(teksti1)
    }

    // TEHTÄVÄ 1.7.B: aseta kenttään "kenttä1" arvo: onBlur-eventissä
    function asetaKentta1_onBlur(event) {
        setTeksti1(event.target.value)
        console.log("onBlur: " +teksti1)
        asetaKentta2_onBlur(teksti1)
    }

    // TEHTÄVÄ 1.7.A: aseta kenttään "kenttä2" arvo: kutsutaan asetaKenttä1:stä onChange:stä
    function asetaKentta2_onChange(teksti1) {
        if(teksti1 === "teksti" && teksti2 === "") {
            setTeksti2(teksti1 + teksti1)
        }
    }
    
    // TEHTÄVÄ 1.7.B: aseta kenttään "kenttä2" arvo: kutsutaan asetaKenttä1:stä onBlur:stä
    function asetaKentta2_onBlur(teksti1) {
        if(teksti1 !== "" && teksti2 === "") {
            setTeksti2(teksti1 + teksti1)
        }
    }

  return (
    <div className="App">
        <header className="App-header">         
            <p>
            <code>Tämä on KayttoLiittyma17-komponentti</code>
            </p>
        </header>

        <div>
            kenttä1:
            <input type="text" id="kenttä1" name="kenttä1" value={teksti1}
                onChange={asetaKentta1_onChange}
                onBlur={asetaKentta1_onBlur}
            >
            </input>
        </div>
        <div>
            kenttä2:
            <input type="text" id="kenttä2" name="kenttä2" 
                value={teksti2}
            >
            </input>

            {/* AO: Crashaa sovelluksen :=) TOO MANY RENDERS, 26 STACKS COLLAPSED

            <button onClick={setTeksti2("")}>
                Tyhjää
            </button>

            */}
        </div>   

    </div>  
    
  );
}

export default KayttoLiittyma17;