// UNDERSCORE-kirjasto
//
// paketin asennus: npm install underscore.
// - otetaan muuttuja _ käyttöön (underscore)
var _ = require('underscore');

/* 
ESIMERKKEJÄ

JavaScript-funktiot ja Underscore-kirjaston vastaavat funktiot
*/

lukuja = [1, 8, 3, 4, 5, 6];
uustaulukko = []; // uustaulukko tiedon muuttamista varten
console.log("alkuperäinen taulukko: " + lukuja);
console.log("Lisätään alkuperäisen taulukon lukuja kahdella + 2")

// (1) forEach / JavaScript
/* 
TULLUT JAVASCRIPTIIN:
Standard ECMA-262
6th Edition / June 2015
*/
console.log("(1) forEach-metodi / JavaScript")
lukuja.forEach( element => {
    uustaulukko.push(element+2);
})
console.log(uustaulukko);

// (1) _.each / Underscore.js
// _.each(list, iteratee, [context]) Alias: forEach
/* OLLUT HYÖDYLLINEN ENNEN KUIN TULI JavaScriptiin 06/2015
Iterates over a list of elements, yielding each in turn to an iteratee function.
The iteratee is bound to the context object, if one is passed. 
Each invocation of iteratee is called with three arguments: (element, index, list). 
If list is a JavaScript object, iteratee's arguments will be (value, key, list). 
Returns the list for chaining. 
*/
uustaulukko = [];
console.log("(1) _each -metodi underscore-kirjastossa");
console.log("Alkuperäinen 'lukuja'-taulukko:")
console.log(lukuja)
_.each(lukuja, element => {
    uustaulukko.push(element+2);
})
console.log("'uustaulukko' lukuja lisätty + 2:")
console.log(uustaulukko);


// (2) map-funktio / JavaScript
/* 
TULLUT JAVASCRIPTIIN:
Standard ECMA-262
5.1 Edition / June 2011
*/
uustaulukko = [];
console.log("(2) map-metodi / JavaScript")
console.log("Alkuperäinen 'lukuja'-taulukko:")
console.log(lukuja)
uustaulukko = lukuja.map( value => value+2 );
console.log("'uustaulukko' lukuja lisätty + 2:")
console.log(uustaulukko);

// (2) _.map / Underscore.js:
/* OLLUT HYÖDYLLINEN ENNEN KUIN TULI JavaScriptiin 06/2011
_.map(list, iteratee, [context]) Alias: collect
Produces a new array of values by mapping each value in list through a transformation function (iteratee).
The iteratee is passed three arguments: the value, then the index (or key) of the iteration, 
and finally a reference to the entire list. 
*/
//_.map([1, 2, 3], function(num){ return num * 3; });
console.log("(2) _map-metodi / underscore-kirjasto")
console.log("Alkuperäinen 'lukuja'-taulukko:")
console.log(lukuja)
uustaulukko = _.map(lukuja, value => value+2);
console.log("'uustaulukko' lukuja lisätty + 2:")
console.log(uustaulukko);


// (3) _.groupBy / Underscore.js
console.log("(3) _.groupBy / Underscore.js")
luvut = ['one', 'two', 'three', 'four'];
console.log("Taulukko 'luvut':")
console.log(luvut)
// Luokitellaan lukusanat sanan pituuden mukaan taulukkoon (esim. 'one' pituus on 3)
luvutLuokiteltu = [];
luvutLuokiteltu = _.groupBy(luvut, 'length');
console.log("'luvut' luokiteltuna lukusanan pituuden mukaan taulukkoon:")
console.log(luvutLuokiteltu)

// (3) groupBy-toteutus: Reduce / JavaScript
/*
REDUCE TULLUT JAVASCRIPTIIN:
Standard ECMA-262
5.1 Edition / June 2011
*/
luvutLuokiteltu = [];
console.log("(3) groupBy-toteutus: Reduce / JavaScript:");
console.log("Taulukko 'luvut':")
console.log(luvut)
// luokittelufunktio:
function groupBy(objectArray, property) {
    return objectArray.reduce(function (acc, obj) {
      var key = obj[property];
      if (!acc[key]) {
        acc[key] = [];
      }
      acc[key].push(obj);
      return acc;
    }, {});
  }
luvutLuokiteltu = groupBy(luvut, 'length');
console.log("'luvut' luokiteltuna lukusanan pituuden mukaan taulukkoon:")
console.log(luvutLuokiteltu)

// (4) _.sortBy / Underscore.js
console.log("(4) _.sortBy / Underscore.js")
var henkilöt = [{name: 'Risto', age: 40}, {name: 'Pekka', age: 18}, {name: 'Aaro', age: 60}];
console.log("Henkilöobjektitaulukko 'henkilöt':")
console.log(henkilöt)
henkilötNimenMukaan = [];
// Returns a (stably) sorted copy of list !!!!
henkilötNimenMukaan = _.sortBy(henkilöt, 'name');
console.log("_.sortBy-funktio / underscore-kirjasto: sorttaus henkilön nimen mukaan");
console.log("Uusi objektitaulukko 'henkilötNimenMukaan:'");
console.log(henkilötNimenMukaan);

// (4) sortBy-toteutus: Sort / JavaScript
/*
SORT TULLUT JAVASCRIPTIIN:
Standard ECMA-262
5.1 Edition / June 2011
*/
// sort by name
console.log("(4) sortBy-toteutus: Sort / JavaScript:");
console.log("Henkilöobjektitaulukko 'henkilöt':")
console.log(henkilöt)
// Note that the array is sorted in place, and no copy is made.!!!!
henkilöt.sort(function(a, b) {
    var nameA = a.name.toUpperCase(); // ignore upper and lowercase
    var nameB = b.name.toUpperCase(); // ignore upper and lowercase
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    } 
    // names must be equal
    return 0;
  });
  console.log("Note that the array is sorted in place, and no copy is made.!")
console.log("Eli alkuperäinen objektitaulukko 'henkilöt' sortattuna nimen mukaan:");
console.log(henkilöt);


