// Tuukka Hyvärisen ratkaisu

var nimekkeet = [];
var arvot = []; 

document.getElementById("nimekkeet").innerHTML = "Toistot: [ ";
document.getElementById("arvot").innerHTML = "Aika: [ ";

const testi = n => {

    for (let i = 0; i < n; i++) {
        let temppi;
        let viikonpaiva = Math.floor(Math.random() * (8 - 1)) + 1;
        let viikonpaivat =
            ["maanantai", "tiistai", "keskiviikko", "torstai", "perjantai", "lauantai", "sunnuntai"];
        temppi = viikonpaivat[viikonpaiva - 1];
    }
}

// 
for (n = 10; n <= 100000000; n = n*10) {

    t0 = performance.now();
    testi(n);
    t1 = performance.now();
    time = t1 - t0;
    nimekkeet.push(n);
    document.getElementById("nimekkeet").innerHTML += n + "\t";
    arvot.push(time);
    document.getElementById("arvot").innerHTML += Math.round(time*100) / 100 + "\t";
    console.log("Aikaa meni " + Math.round(time*100) / 100 + " millisekuntia.");

}

document.getElementById("nimekkeet").innerHTML += "]";
document.getElementById("arvot").innerHTML += "]";

function rakennaKaavio() {

    var kaavio = document.getElementById("graafi").getContext('2d');
    var pylvaskaavio = new Chart(kaavio, {

        type: 'bar',
        data: {
            labels: nimekkeet,
            datasets: [{
                label: "aika millisekunteina",
                data: arvot,
                backgroundColor: 'rgba(54, 162, 235, 0.6)'
            }]
        },
        options: {}
    });

}

