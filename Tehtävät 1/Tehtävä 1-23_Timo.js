{ // Constant values

    let density_per_dm = 2.5, truck_max_mass = 10500, elements = 50;
  
    // Calculate element dimensions
  
    var dimensions = Array.from({ length: elements }, (x, i) =>
  
      [3 * 1.020 ** i, 5 * 1.030 ** i, 5 * 1.015 ** i]);
  
    // Calculate element weight from dimensions
  
    var masses = dimensions.map(dim => dim.reduce((a, b) => a * b, density_per_dm));
  
    // Compile truck loads from element size list
  
    var [trucks, last_load] = masses.reduce(([cnt, ld], mss) =>
  
      ld + mss <= truck_max_mass ? [cnt, ld + mss] : [cnt + 1, mss]
  
    , [1, 0]);
  
    // Print the result
  
    print('Rekan tarvitsee ajaa ' + trucks + ' kuormaa');
  
  }