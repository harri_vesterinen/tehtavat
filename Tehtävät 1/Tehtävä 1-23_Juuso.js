alkuarvo = {kerrat:0, rekanPaino:0,  seuraavanKuormanAlkupaino:0}

    tulos = painotLaskevana.reduce((data, esineenPaino) => {

        if (data.rekanPaino + esineenPaino <= 10500 && data.seuraavanKuormanAlkupaino==0) {

            return {    ...data,

                        rekanPaino:data.rekanPaino + esineenPaino}; 

        } else if (data.seuraavanKuormanAlkupaino>0) {

            return {    ...data,

                        rekanPaino:data.seuraavanKuormanAlkupaino + esineenPaino,

                        seuraavanKuormanAlkupaino:0};

        }

        else {

            return {...data,

                    kerrat: data.kerrat+1,

                    rekanPaino:0,

                    seuraavanKuormanAlkupaino:esineenPaino,

                    };

        }

    },alkuarvo)