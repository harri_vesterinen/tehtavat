

"use strict"

const elmWeightKg = (mitat) => {

    let tilavuusMetr = mitat.p * mitat.l * mitat.k

    return 2.5 * 1e3 * tilavuusMetr

}

const getNextElmDimensions = (() => {

    let smallest = {p: 0.3 * (1 / 1.02), l: 0.5 * (1 / 1.03), k: 0.5 * (1 / 1.015)}

    return () => {

        smallest.p *= 1.02

        smallest.l *= 1.03

        smallest.k *= 1.015

        

        return smallest

        }

})()

const getLargestFit = (val, elms) => {

    let elm = elms.reduceRight((a, b) => (a > b && a <= val) ? a : b)

    return (elm <= val) ? elm : null

}

let elms = []

for (let i = 0; i < 50; i++) {

    elms.push(elmWeightKg(getNextElmDimensions()))

}

let maxWeight = 10500,

    kgAccu = 0,

    amtTrips = 0,

    selectElm = null,

    

for (;elms.length > 0;) {

    for (;elms.length > 0;) {

        selectElm = getLargestFit(maxWeight - kgAccu, elms)

        if (selectElm === null)

            break

        else

            kgAccu += elms.splice(elms.indexOf(selectElm), 1)[0]

    }

    amtTrips++

    kgAccu = 0

}

console.log("")

console.log("Trips to construction site: ", amtTrips)

