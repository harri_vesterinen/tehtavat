// HVe

import React, {useState, useEffect} from 'react';
//import logo from './logo.svg';
import './App.css';

//const axios = require('axios').default;
const axios = require('axios'); // axioksen käyttö

function App() {

  const [etunimi, setEtunimi] = useState("")
  // 'expresspalvelin'-hakemiston palvelin: portissa 3000
  //const [url, setUrl] = useState("http://localhost:3000/");

  // 'express-palvelin'-hakemiston palvelin: portissa 3999
  const [url, setUrl] = useState("http://localhost:3999/");

  // kun painetaan nappia 'Lähetä etunimi' ja kun etunimi muuttuu kentässä.
  function lahetaEtunimi(etunimi) {

    axios.post(url, {
      etunimi: etunimi
    })
    .then(function (response) {
      console.log(response);
    })
    .catch(function (error) {
      console.log(error);
    });

    /* Voisi olla myös muotoa:
    
    // Send a POST request
    axios({
      method: 'post',
      url: '/user/12345',
      data: {
        firstName: 'Fred',
        lastName: 'Flintstone'
      }
    });
    
    */

  }

  // kun etunimi muuttuu:
  useEffect( () => {
    console.log("etunimi muuttuu: " + etunimi)
    lahetaEtunimi(etunimi)
    /*
    Poistetaan alla ESLint-virheilmoitus, joka tulee [etunimi]-kohdasta:
      React Hook useEffect has a missing dependency: 'lahetaEtunimi'.
      Either include it or remove the dependency array  react-hooks/exhaustive-deps
    */
  // eslint-disable-next-line
  }, [etunimi])

  return (
    <div className="App">
      
      <h1>Express.js / React testiapplikaatio</h1>
      <input type="text" value={etunimi} onChange={(e) => setEtunimi(e.target.value)}
            onKeyDown={(e) => e.keyCode === 13 ? lahetaEtunimi(etunimi) : setEtunimi(etunimi)} />
      <input type="button" onClick={() => lahetaEtunimi(etunimi)} value="Lähetä etunimi" />
      <br />
      <span id="lahetysUrl">Käytetään URL-osoitetta: {url}</span>
      <br />
      <span>Muuta URL-osoitetta: </span>
      <input type="text" value={url} onChange={(e) => setUrl(e.target.value)} />
    </div>
  );
}

export default App;
