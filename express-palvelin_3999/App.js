const express = require('express')
const app = express()
const port = 3999
var cors = require('cors')

//app.get('/', (req, res) => res.send('Hello World!'))
//app.listen(port, () => console.log(`Example app listening on port ${port}!`))

app.use(cors())
app.use(express.json())

app.post('/', (req, res, next) => {
    console.log(req.body.firstName)
    res.json(req.body)
    })    

app.listen(port, () => console.log(`Example app listening on port ${port}!`))