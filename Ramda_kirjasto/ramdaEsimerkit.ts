// ks. https://ramdajs.com/
/* 
tee: npm install ramda
*/

import { clone, multiply, curry, pipe, compose, sort, path, map, filter, head, mergeDeepLeft } from "ramda";
// tai JavaScriptissä:   const R = require('ramda');

// Aluksi yksinkertainen taulukon deep copy -esimerkki (Vertaa Taulukot tehtävä 2.9-2.13)
let objektit = [{ "ma": 44 }, { "pe": 100 }, { "ke": 21 }, { "ti": 66 }, { "la": 22 }];
let objektitKlooni = clone(objektit)

console.log(objektit);          // > [{ "ma": 44 }, { "pe": 100 }, { "ke": 21 }, { "ti": 66 }, { "la": 22 }];
console.log(objektitKlooni);    // > [{ "ma": 44 }, { "pe": 100 }, { "ke": 21 }, { "ti": 66 }, { "la": 22 }]; 

// clone-funktio luo syväkopion, joten alkuperäisen taulukon arvon muutos ei vaikuta kloonattuun taulukkoon
objektit[0].ma = 22;

console.log(objektit);          // > [{ "ma": 22 }, { "pe": 100 }, { "ke": 21 }, { "ti": 66 }, { "la": 22 }];
console.log(objektitKlooni);    // > [{ "ma": 44 }, { "pe": 100 }, { "ke": 21 }, { "ti": 66 }, { "la": 22 }];

// Vrt. perinteisen JavaScriptin yksi toteutustapa
let objektitKlooni2 = JSON.parse(JSON.stringify(objektit));




// Kaikki Ramdan funktiot ovat auto-curryutuvia. Tässä kertomiseen käytettävä multiply

console.log(multiply(2, 5));    // > 10
console.log(multiply(2)(5));    // > 10
const triplaa = multiply(3); 
console.log(triplaa(5));        // > 15





// Ramda sisältää myös curry-funktion, jolla oma funktio voidaan muuttaa currytuvaksi

const kokoNimi = (etunimi, sukunimi) => sukunimi + ", " + etunimi;
console.log(kokoNimi("Edu", "Kettunen"));           // > Kettunen, Edu

// console.log(kokoNimi("Edu")("Kettunen"));           // TypeError: kokoNimi(...) is not a function.

const curriedKokoNimi = curry(kokoNimi);
console.log(curriedKokoNimi("Edu", "Kettunen"));    // > Kettunen, Edu
console.log(curriedKokoNimi("Edu")("Kettunen"));    // > Kettunen, Edu





// Miksi Ramda mieluummin kuin funktiokutsujen ketjuttaminen?  

const nelio = x => x * x;
const puolita = x => x / 2;
const lisaaYksi = x => x + 1;

const laskeAsioita = x => puolita(lisaaYksi(triplaa(nelio(x))));

console.log(laskeAsioita(4));       /*  > puolita(lisaaYksi(triplaa(nelio(4))))
                                        > puolita(lisaaYksi(triplaa(16)))
                                        > puolita(lisaaYksi(48))
                                        > puolita(49)
                                        > 24.5 */

// Useiden sisäkkäisten funktioiden kirjoittaminen ja lukeminen voi muuttua työlääksi.
// Ramda tarjoaa vaihtoehdon funktioilla pipe ja compose

const laskeAsioitaPipe = pipe(nelio, triplaa, lisaaYksi, puolita);
console.log(laskeAsioitaPipe(4));   // > 24.5

// pipe-funktiota on helppo lukea, turhat sulkeet ovat poissa ja funktioiden kutsut tapahtuvat vasaemmalta oikealle.

// compose-funktio tekee saman kuin pipe, mutta muuttaa funktioiden suoritusjärjestyksen vastapäiseen eli oikealta vasemmalle

compose(puolita, lisaaYksi, triplaa, nelio) 
//  == pipe(nelio, triplaa, lisaaYksi, puolita);


// compose-funktio esittää funktioiden yhdistämisen matemaattisempaa notaatiota käyttäen (f o g o h)(x) = f(g(h(x))))



// Ramda tarjoaa useita JavaScriptin kanssa samankaltaisia toimintoja.
// Ramda kuitenkin noudattaa usein paremmin funktionaalisen ohjelmoinnin muuttumattoman datan periaatetta.
// Esimerkiksi sort-funktiot

// JavaScriptin array.sort-funktio
const lista1 = [5, 2, 7, 1, 3, 9, 6];
lista1.sort((x, y) => x - y);
console.log(lista1);                // > [1, 2, 3, 5, 6, 7, 9]

// Ramdan sort ei muokkaa alkuperäistä taulukkoa.
const lista2 = [5, 2, 7, 1, 3, 9, 6];
const jarjestettyLista2 = sort((x, y) => x - y, lista2);  
console.log(jarjestettyLista2);     // > [1, 2, 3, 5, 6, 7, 9]
console.log(lista2);                // > [5, 2, 7, 1, 3, 9, 6]





// Ramdasta löytyy myös hyödyllinen path-funktio sisäkkäisten rakenteiden (lista/object) arvojen käsittelemiseen

let objektitSisakkain = { a: { b: { c: 5 } } };
console.log(path(['a', 'b', 'c'], objektitSisakkain)); // > 5



// Tosimaailman esimerkki, missä nähdään kuinka Ramdan avulla voidaan helposti käsitellä sisäkkäistä dataa.
// Olkoon saatu data seuraavaa muotoa
let data = {
    "tulos": {
        "kirjailijat": [
            { "id": 101, "nimi": "George", "puhelinnumero": "016-111234" },
            { "id": 124, "nimi": "Erlend", "puhelinnumero": "014-999876" }
        ],
        "kirjat": [
            { "id": 434, "otsikko": "Supernaiivi", "kirjailija": 124 },
            { "id": 382, "otsikko": "Valtaistuinpeli", "kirjailija": 101 }
        ]
    }
}

// Mitä jos haluasimme saada kaikkien kirjalijoiden nimet taulukkoon
// Ramdan toteutus

const haeKirjailijoidenNimet = pipe(
    path(['tulos', 'kirjailijat']),
    map(kirjailija => kirjailija.nimi)
);
haeKirjailijoidenNimet(data); // > [George, Erlend];



// Vastaava toteutus perinteisellä JavaScriptillä
console.log(data.tulos.kirjailijat.map(kirjailija => kirjailija.nimi)); // > [George, Erlend]


// Hieman monimutkaisempi tosimaailman esimerkki Ramdalla
// Yhdistetään datan kirjailijat ja kirjat.

const haeKirjailija = (id) => pipe(
    path(['tulos', 'kirjailijat']),              // Haetaan kirjailijat lista
    filter(kirjailija => kirjailija.id === id),  // Valitaan kirjailijat, joiden id vastaa annettua                                    
    head                                         // Otetaan saadun listan ensimmäinen alkio.
);

console.log(haeKirjailija(124)(data)); // > {id: 124, nimi: "Erlend", puhelinnumero: "014-999876"};

const haeYhdistelma = pipe(
    path(['tulos', 'kirjat']),                                      // Haetaan kirjat listas
    map(kirja =>                                                    // Luodaan kirjat listasta uusi lista, jossa
        mergeDeepLeft(kirja, haeKirjailija(kirja.kirjailija)(data)) // kirja-objektin sisään on yhdistetty sitä vastaava
    )                                                               // kirjailija-objekti.
)

console.log(haeYhdistelma(data));     // > [ {id: 434, kirjailija: 124, nimi: "Erlend", otsikko: "Supernaiivi", puhelinnumero: "014-999876"},
                                      //     {id: 382, kirjailija: 101, nimi: "George", otsikko: "Valtaistuinpeli", puhelinnumero: "016-111234"}]



