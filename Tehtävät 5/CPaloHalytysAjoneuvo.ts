/*
Tehtävä 5.4
Periytä CHalytysAjoneuvo luokasta uusi luokka CPaloHalytysAjoneuvo. Lisää uuteen
luokkaan ainakin paloautoille tutut ominaisuudet, jotka kuvaavat auton tikkaiden korkeutta
ja autossa olevien sammutusletkuliittimien lukumäärää. Tee ominaisuuksille get ja set -
metodit. Testaa myös uuden luokkasi toiminnot ja varmista, että luokkasi toimii.
*/

// ******************************************
// * Start of class CPaloHalytysAjoneuvo    *
// ******************************************
class CPaloHalytysAjoneuvo extends CHalytysAjoneuvo {

    // ******************
    // *   properties   *
    // ******************
    protected _iTikasKorkeus: number;
    protected _iSammutusLetkuLiittimienLkm: number;
    protected _iVesiSailionTilavuusLtr: number; // Tehtävä 5.6: vesisäiliön tilavuus max ltr
    protected _iVedenMaaraJaljellaLtr: number; // Tehtävä 5.6.: käytössä olevan veden määrä (onko vettä vai ei)

    // ******************************************
    // *    constructor                         *
    // ******************************************
    // ------------------------

    // *******************************************
    // ***** methods: set and get properties *****
    // *******************************************

    // iTikasKorkeus
    set iTikasKorkeus(uusi_iTikasKorkeus: number) {
        this._iTikasKorkeus = uusi_iTikasKorkeus;
    }
    get iTikasKorkeus(): number {
        return this._iTikasKorkeus;
    }
    // iSammutusLetkuLiittimienLkm
    set iSammutusLetkuLiittimienLkm(uusi_iSammutusLetkuLiittimienLkm: number) {
        this._iSammutusLetkuLiittimienLkm = uusi_iSammutusLetkuLiittimienLkm;
    }
    get iSammutusLetkuLiittimienLkm(): number {
        return this._iSammutusLetkuLiittimienLkm;
    }


    /*
    Tehtävä 5.6 / OK
    Tee palohälytysajoneuvolle uusi ominaisuus, joka kuvaa vesisäiliön tilavuutta (OK). Lisää myös
    uusi metodi, jonka avulla voi selvittää, onko vesisäiliössä vettä vai ei (OK).
    Tee myös metodi, joka kuvaa tilannetta, jossa vesisäiliöstä ruiskutetaan x litraa vettä pois (OK).
    Tee myös metodit, joiden avulla pois ruiskutettavan veden määrä voidaan antaa myös
    Englannin ja Amerikan gallonissa (OK). 1 litra on 0.22 Englannin gallonaa ja 0.26 Amerikan
    gallonaa.
    Kun vesisäiliö on tyhjä, tulee vesisäiliön tilaa(vettä on/ei ole) kuvaavan ominaisuuden
    arvoa muuttaa (OK). Lisää vielä palohälytysajoneuvolle metodi, jolla voidaan selvittää onko
    vesisäiliössä vettä vai ei (OK).
    */

    // iVesiSailionTilavuusLtr
    set iVesiSailionTilavuusLtr(iMaxMaara: number) {
        this._iVesiSailionTilavuusLtr = iMaxMaara;
    }
    get iVesiSailionTilavuusLtr(): number {
        return this._iVesiSailionTilavuusLtr;
    }

    // iVedenMaaraJaljellaLtr
    set iVedenMaaraJaljellaLtr(uusiVedenMaara: number) {
        this._iVedenMaaraJaljellaLtr = uusiVedenMaara;
    }
    get iVedenMaaraJaljellaLtr(): number {
        return this._iVedenMaaraJaljellaLtr;
    }

    // metodi: vähennä vettä säiliöstä ltr
    public vahennaVettaLtr(vahennaVettaLtr: number) {
        if(this.iVedenMaaraJaljellaLtr - vahennaVettaLtr <= 0) {
            this.iVedenMaaraJaljellaLtr = 0;
        } else {
            this.iVedenMaaraJaljellaLtr -= vahennaVettaLtr;
        }
    }

    // metodi: onko vesisäiliössä vettä vai ei (palauttaa True/False)
    public bOnkoVetta() {
        if(this.iVedenMaaraJaljellaLtr > 0) {
            return true;
        } else {
            return false;
        }
    }

    // metodi: vähennä vettä säiliöstä englannin gallonat
    public vahennaVettaENGGallons(vahennaVettaENGGallons: number) {
        // 1 litra on 0.22 Englannin gallonaa
        let maaraLitroissa = vahennaVettaENGGallons / 0.22;
        if(this.iVedenMaaraJaljellaLtr - maaraLitroissa <= 0) {
            this.iVedenMaaraJaljellaLtr = 0;
        } else {
            this.iVedenMaaraJaljellaLtr -= maaraLitroissa;
        }
    }

    // metodi: vähennä vettä säiliöstä amerikan gallonat
    public vahennaVettaUSGallons(vahennaVettaUSGallons: number) {
        // 1 litra on 0.26 Amerikan gallonaa
        let maaraLitroissa = vahennaVettaUSGallons / 0.26;
        if(this.iVedenMaaraJaljellaLtr - maaraLitroissa <= 0) {
            this.iVedenMaaraJaljellaLtr = 0;
        } else {
            this.iVedenMaaraJaljellaLtr -= maaraLitroissa;
        }
    }

    /*
    Tehtävä 5.7 / OK
    Lisää palohälytysajoneuvolle metodi, jonka avulla vesisäiliötä voidaan täyttää. Vesisäiliö ei
    kuitenkaan saa täyttyä yli äyräidensä; vettä voi olla enintään vesisäiliön tilavuuden verran.
    */

    // metodi: lisää vettä säiliöön ltr
    public lisaaVettaLtr(lisaaVettaLtr: number) {
        if(this.iVedenMaaraJaljellaLtr + lisaaVettaLtr > this.iVesiSailionTilavuusLtr) {
            this.iVedenMaaraJaljellaLtr = this.iVesiSailionTilavuusLtr;
        } else {
            this.iVedenMaaraJaljellaLtr += lisaaVettaLtr;
        }
    }

    /*
    Tehtävä 5.8 / OK
    Mikä on mielestäsi olio-ohjelmoinnin oleellisin hyöty ohjelmistokehityksessä - mainitse
    perustellen ainakin kolme asiaa?

    1) helpompi hahmottaa reaalimaailmaa sellaisena kuin se on

    2) kaikki tarvittava on pakattu olion sisälle (propertyt ja metodit)
    
    3) public / private / protected -määreiden avulla pystyy säätämään, mitä halutaan
        milloinkin näyttää minnekin tai voiko niitä muuttaa ja mistä käsin.

    */

}
// *****************************************
// *   End of class CPaloHalytysAjoneuvo   *
// *****************************************


// TÄSTÄ KYSYTTÄVÄ!!!!!

// Ohjelma, jolla testataan PaloHalytysAjoneuvo-luokkaa
// 
let paloAuto = new CPaloHalytysAjoneuvo("Volvo");
paloAuto.iIskutilavuus = 5;
paloAuto.iHinta = 250000;
paloAuto.iOvienLkm = 6;
paloAuto.bKattoikkuna = false;
paloAuto.iAjetutKilometrit = 12000;
paloAuto.sMerkki = "Volvo";
paloAuto.iValmistusvuosi = 2018;
paloAuto.dMoottorinIskutilavuusLtr = 5000;

console.log("paloAuto: Merkki:" + paloAuto.sMerkki + ", iskutilavuus:" + paloAuto.iIskutilavuus
    + ", hinta: "+ paloAuto.iHinta);

// MIKSEI AO: TOIMI ?????
//console.log("Auto1: ovien lkm: " + auto1.iOvienLkm + ", kattoikkuna:" + auto1.bKattoikkuna);

console.log("paloAuto: ajetut km: " + paloAuto.iAjetutKilometrit + ", valm.vuosi: " + paloAuto.iValmistusvuosi
+ ", moottorin til LTR: " + paloAuto.dMoottorinIskutilavuusLtr);

console.log("paloAuto kaikki tiedot tulostettu ok.");

//*********************************** */
//export default CPaloHalytysAjoneuvo;