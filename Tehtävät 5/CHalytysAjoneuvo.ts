/*
Tehtävä 5.3
Periytä CHalytysAjoneuvo luokasta CAjoneuvo ja lisää ainakin kolme uutta ominaisuutta,
jotka kuvaavat sitä, onko ajoneuvossa tietty hälytysääni vai ei, onko hälytysajoneuvossa
radiopuhelimet vai ei ja hälytysajoneuvon korkeus metreinä. Luo em. ominaisuuksille get ja
set -metodit.
*/

//import './CAjoneuvo.js';
//import {CAjoneuvo} from './CAjoneuvo'

// KS EXPORT ALLA ????

// **************************************
// * Start of class CHalytysAjoneuvo    *
// **************************************
export class CHalytysAjoneuvo extends CAjoneuvo {

    // ******************
    // *   properties   *
    // ******************
    protected _bHalytysAani: boolean;
    protected _bRadioPuhelimet: boolean;
    protected _iKorkeus: number;

    // ******************************************
    // *    constructor                         *
    // ******************************************
    // ------------------------

    // *******************************************
    // ***** methods: set and get properties *****
    // *******************************************

    // bHalytysAani
    set bHalytysAani(uusi_bHalytysAani: boolean) {
        this._bHalytysAani = uusi_bHalytysAani;
    }
    get bHalytysAani(): boolean {
        return this._bHalytysAani;
    }
    // bRadioPuhelimet
    set bRadioPuhelimet(uusi_bRadioPuhelimet: boolean) {
        this._bRadioPuhelimet = uusi_bRadioPuhelimet;
    }
    get bRadioPuhelimet(): boolean {
        return this._bRadioPuhelimet;
    }
    // iKorkeus
    set iKorkeus(uusi_iKorkeus: number) {
        this._iKorkeus = uusi_iKorkeus;
    }
    get iKorkeus(): number {
        return this._iKorkeus;
    }

}
// *************************************
// *   End of class CHalytysAjoneuvo   *
// *************************************

let halyauto1 = new CHalytysAjoneuvo("Hälyauto");
halyauto1.iIskutilavuus = 1;
halyauto1.iHinta = 2300;
//auto1.iOvienLkm = 4;
//auto1.bKattoikkuna = false;
//auto1.iAjetutKilometrit = 83000;
//auto1.sMerkki = "Suzuki";
//auto1.iValmistusvuosi = 2004;
//auto1.dMoottorinIskutilavuusLtr = 900;

console.log("halyauto1: Merkki:" + halyauto1.sMerkki + ", iskutilavuus:" + halyauto1.iIskutilavuus
    + ", hinta: "+ halyauto1.iHinta);

// MIKSEI AO: TOIMI ?????
//console.log("Auto1: ovien lkm: " + auto1.iOvienLkm + ", kattoikkuna:" + auto1.bKattoikkuna);

//console.log("Auto1: ajetut km: " + auto1.iAjetutKilometrit + ", valm.vuosi: " + auto1.iValmistusvuosi
//+ ", moottorin til LTR: " + auto1.dMoottorinIskutilavuusLtr);

console.log("Auto1 kaikki tiedot tulostettu ok.");


//*  **************************************
//export default CHalytysAjoneuvo;