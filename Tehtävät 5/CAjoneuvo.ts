/* 
Tehtävä 5.1
Laadi oma luokka, jonka nimi on CAjoneuvo. Luokalla tulee olla ominaisuuksina ainakin
seuraavat iIskutilavuus(kokonaisluku), iHinta(kokonaisluku), iOvienLkm(kokonaisluku),
bKattoikkuna(boolean), iAjetutKilometrit(kokonaisluku), sMerkki(merkkijono),
iValmistusvuosi(kokonaisluku) ja dMoottorinIskutilavuusLtr(desimaaliluku).
Kaikille ominaisuuksille tulee laatia set metodi. Lisäksi jokaiselle ominaisuudelle pitää
laatia get metodi, joka palauttaa ominaisuuden arvon.
*/

// ******************************
// * Start of class CAjoneuvo    *
// ******************************
class CAjoneuvo {

    // ******************
    // *   properties   *
    // ******************
    protected _iIskutilavuus: number;
    protected _iHinta: number;
    protected _iOvienLkm: number;
    protected _bKattoikkuna: boolean;
    protected _iAjetutKilometrit: number;
    protected _sMerkki: string;
    protected _iValmistusvuosi: number;
    protected _dMoottorinIskutilavuusLtr: number;

    // ******************************************
    // *    constructor                         *
    // ******************************************
    constructor(_sMerkki: string) { 
        this._sMerkki = _sMerkki;
        this._sMerkki = _sMerkki;
    }

    // *******************************************
    // ***** methods: set and get properties *****
    // *******************************************

    // _iIskutilavuus
    set iIskutilavuus(uusi_iIskutilavuus: number) {
        this._iIskutilavuus = uusi_iIskutilavuus;
    }
    get iIskutilavuus(): number {
        return this._iIskutilavuus;
    }
    // _iHinta
    set iHinta(uusi_iHinta: number) {
        this._iHinta = uusi_iHinta;
    }
    get iHinta(): number {
        return this._iHinta;
    }
    // _iOvienLkm
    set iOvienLkm(uusi_iOvienLkm: number) {
        this._iOvienLkm = uusi_iOvienLkm;
    }
    get iOvienLkm(): number {
        return this.iOvienLkm;
    }
    // bKattoikkuna
    set bKattoikkuna(uusi_bKattoikkuna: boolean) {
        this._bKattoikkuna = uusi_bKattoikkuna;
    }
    get bKattoikkuna(): boolean {
        return this._bKattoikkuna;
    }
    // iAjetutKilometrit
    set iAjetutKilometrit(uusi_iAjetutKilometrit: number) {
        this._iAjetutKilometrit = uusi_iAjetutKilometrit;
    }
    get iAjetutKilometrit(): number {
        return this._iAjetutKilometrit;
    }
    // sMerkki
    set sMerkki(uusi_sMerkki: string) {
        this._sMerkki = uusi_sMerkki;
    }
    get sMerkki(): string {
        return this._sMerkki;
    }
    // iValmistusvuosi
    set iValmistusvuosi(uusi_iValmistusvuosi: number) {
        this._iValmistusvuosi = uusi_iValmistusvuosi;
    }
    get iValmistusvuosi(): number {
        return this._iValmistusvuosi;
    }
    // dMoottorinIskutilavuusLtr
    set dMoottorinIskutilavuusLtr(uusi_dMoottorinIskutilavuusLtr: number) {
        this._dMoottorinIskutilavuusLtr = uusi_dMoottorinIskutilavuusLtr;
    }
    get dMoottorinIskutilavuusLtr(): number {
        return this._dMoottorinIskutilavuusLtr;
    }

}
// ******************************
// *   End of class CAjoneuvo   *
// ******************************


/*
Tehtävä 5.2
Tee ohjelma, joka käyttää hyväkseen edellisen harjoituksen CAjoneuvo-luokkaa.
Ohjelmassa tulee luoda ainakin kaksi uutta auto-oliota ja antaa olioiden ominaisuuksille
arvot. Testaa, että kaikki metodisi toimivat oikein eli palauttavat oikeita arvoja ja antavat
ominaisuuksille oikein oikeat arvot.
*/

// Ohjelma, jolla testataan ajoneuvoja
// Auto 1
let auto1 = new CAjoneuvo("");
auto1.iIskutilavuus = 1;
auto1.iHinta = 2300;
auto1.iOvienLkm = 4;
auto1.bKattoikkuna = false;
auto1.iAjetutKilometrit = 83000;
auto1.sMerkki = "Suzuki";
auto1.iValmistusvuosi = 2004;
auto1.dMoottorinIskutilavuusLtr = 900;

console.log("Auto1: Merkki:" + auto1.sMerkki + ", iskutilavuus:" + auto1.iIskutilavuus
    + ", hinta: "+ auto1.iHinta);

// MIKSEI AO: TOIMI ?????
//console.log("Auto1: ovien lkm: " + auto1.iOvienLkm + ", kattoikkuna:" + auto1.bKattoikkuna);

console.log("Auto1: ajetut km: " + auto1.iAjetutKilometrit + ", valm.vuosi: " + auto1.iValmistusvuosi
+ ", moottorin til LTR: " + auto1.dMoottorinIskutilavuusLtr);

console.log("Auto1 kaikki tiedot tulostettu ok.");

// Auto2
let auto2 = new CAjoneuvo("");
auto2.iIskutilavuus = 2;
auto2.iHinta = 23000;
auto2.iOvienLkm = 2;
auto2.bKattoikkuna = true;
auto2.iAjetutKilometrit = 28000;
auto2.sMerkki = "Ford";
auto2.iValmistusvuosi = 2016;
auto2.dMoottorinIskutilavuusLtr = 2000;

console.log("Auto2: Merkki:" + auto2.sMerkki + ", iskutilavuus:" + auto2.iIskutilavuus
    + ", hinta: "+ auto2.iHinta);

// MIKSEI AO: TOIMI ?????
//console.log("Auto1: ovien lkm: " + auto1.iOvienLkm + ", kattoikkuna:" + auto1.bKattoikkuna);

console.log("Auto2: ajetut km: " + auto2.iAjetutKilometrit + ", valm.vuosi: " + auto2.iValmistusvuosi
+ ", moottorin til LTR: " + auto2.dMoottorinIskutilavuusLtr);

console.log("Auto2 kaikki tiedot tulostettu ok.");


/*
Tehtävä 5.5

© Ideal Learning Ltd 2018, All Rights Reserved
Onko CAjoneuvo-luokan ominaisuuksia, kuten bKattoluukku, mahdollista määritellä niin,
että se ei ole käytettävissä perivissä luokissa? Missä JavaScript (ECMAScript) versioissa
se on tai ei ole mahdollista? Onko se mahdollista TypeScriptissä tai Java-kielessä?

VASTAUS:
Voidaan tehdä myös private-metodeja tai propertyjä -> tällöin se käytössä vain tässä luokassa, eikä
ole käytettävissä aliluokissa.
- JavaScript -> ei voi käyttää private-määrettä
- TypeScript -> voi käyttää private-määrettä
- Jave -> varmaankin voi käyttää private-määrettä


*/


//**************************************** */
//export default CAjoneuvo;

    
