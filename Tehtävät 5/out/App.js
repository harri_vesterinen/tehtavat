// TÄÄ EI TOIMI JOSTAIN SYYSTÄ ????

/*
Tehtävä 5.2
Tee ohjelma, joka käyttää hyväkseen edellisen harjoituksen CAjoneuvo-luokkaa.
Ohjelmassa tulee luoda ainakin kaksi uutta auto-oliota ja antaa olioiden ominaisuuksille
arvot. Testaa, että kaikki metodisi toimivat oikein eli palauttavat oikeita arvoja ja antavat
ominaisuuksille oikein oikeat arvot.
*/

import './CAjoneuvo';
import './CHalytysAjoneuvo';
import './CPaloHalytysAjoneuvo';

// Ohjelma, jolla testataan ajoneuvoja

var auto1 = new CAjoneuvo("Suzuki");
auto1.iIskuTilavuus = 1000;

console.log(auto1)
