/*
Tehtävä 5.3
Periytä CHalytysAjoneuvo luokasta CAjoneuvo ja lisää ainakin kolme uutta ominaisuutta,
jotka kuvaavat sitä, onko ajoneuvossa tietty hälytysääni vai ei, onko hälytysajoneuvossa
radiopuhelimet vai ei ja hälytysajoneuvon korkeus metreinä. Luo em. ominaisuuksille get ja
set -metodit.
*/
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
//import './CAjoneuvo.js';
// **************************************
// * Start of class CHalytysAjoneuvo    *
// **************************************
var CHalytysAjoneuvo = /** @class */ (function (_super) {
    __extends(CHalytysAjoneuvo, _super);
    function CHalytysAjoneuvo() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(CHalytysAjoneuvo.prototype, "bHalytysAani", {
        get: function () {
            return this._bHalytysAani;
        },
        // ******************************************
        // *    constructor                         *
        // ******************************************
        // ------------------------
        // *******************************************
        // ***** methods: set and get properties *****
        // *******************************************
        // bHalytysAani
        set: function (uusi_bHalytysAani) {
            this._bHalytysAani = uusi_bHalytysAani;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CHalytysAjoneuvo.prototype, "bRadioPuhelimet", {
        get: function () {
            return this._bRadioPuhelimet;
        },
        // bRadioPuhelimet
        set: function (uusi_bRadioPuhelimet) {
            this._bRadioPuhelimet = uusi_bRadioPuhelimet;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CHalytysAjoneuvo.prototype, "iKorkeus", {
        get: function () {
            return this._iKorkeus;
        },
        // iKorkeus
        set: function (uusi_iKorkeus) {
            this._iKorkeus = uusi_iKorkeus;
        },
        enumerable: true,
        configurable: true
    });
    return CHalytysAjoneuvo;
}(CAjoneuvo));
// *************************************
// *   End of class CHalytysAjoneuvo   *
// *************************************
var halyauto1 = new CHalytysAjoneuvo("Hälyauto");
halyauto1.iIskutilavuus = 1;
halyauto1.iHinta = 2300;
//auto1.iOvienLkm = 4;
//auto1.bKattoikkuna = false;
//auto1.iAjetutKilometrit = 83000;
//auto1.sMerkki = "Suzuki";
//auto1.iValmistusvuosi = 2004;
//auto1.dMoottorinIskutilavuusLtr = 900;
console.log("halyauto1: Merkki:" + halyauto1.sMerkki + ", iskutilavuus:" + halyauto1.iIskutilavuus
    + ", hinta: " + halyauto1.iHinta);
// MIKSEI AO: TOIMI ?????
//console.log("Auto1: ovien lkm: " + auto1.iOvienLkm + ", kattoikkuna:" + auto1.bKattoikkuna);
//console.log("Auto1: ajetut km: " + auto1.iAjetutKilometrit + ", valm.vuosi: " + auto1.iValmistusvuosi
//+ ", moottorin til LTR: " + auto1.dMoottorinIskutilavuusLtr);
console.log("Auto1 kaikki tiedot tulostettu ok.");
//*  **************************************
//export default CHalytysAjoneuvo;
//# sourceMappingURL=CHalytysAjoneuvo.js.map