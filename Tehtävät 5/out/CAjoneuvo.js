/*
Tehtävä 5.1
Laadi oma luokka, jonka nimi on CAjoneuvo. Luokalla tulee olla ominaisuuksina ainakin
seuraavat iIskutilavuus(kokonaisluku), iHinta(kokonaisluku), iOvienLkm(kokonaisluku),
bKattoikkuna(boolean), iAjetutKilometrit(kokonaisluku), sMerkki(merkkijono),
iValmistusvuosi(kokonaisluku) ja dMoottorinIskutilavuusLtr(desimaaliluku).
Kaikille ominaisuuksille tulee laatia set metodi. Lisäksi jokaiselle ominaisuudelle pitää
laatia get metodi, joka palauttaa ominaisuuden arvon.
*/
// ******************************
// * Start of class CAjoneuvo    *
// ******************************
var CAjoneuvo = /** @class */ (function () {
    // ******************************************
    // *    constructor                         *
    // ******************************************
    function CAjoneuvo(_sMerkki) {
        this._sMerkki = _sMerkki;
        this._sMerkki = _sMerkki;
    }
    Object.defineProperty(CAjoneuvo.prototype, "iIskutilavuus", {
        get: function () {
            return this._iIskutilavuus;
        },
        // *******************************************
        // ***** methods: set and get properties *****
        // *******************************************
        // _iIskutilavuus
        set: function (uusi_iIskutilavuus) {
            this._iIskutilavuus = uusi_iIskutilavuus;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "iHinta", {
        get: function () {
            return this._iHinta;
        },
        // _iHinta
        set: function (uusi_iHinta) {
            this._iHinta = uusi_iHinta;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "iOvienLkm", {
        get: function () {
            return this.iOvienLkm;
        },
        // _iOvienLkm
        set: function (uusi_iOvienLkm) {
            this._iOvienLkm = uusi_iOvienLkm;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "bKattoikkuna", {
        get: function () {
            return this._bKattoikkuna;
        },
        // bKattoikkuna
        set: function (uusi_bKattoikkuna) {
            this._bKattoikkuna = uusi_bKattoikkuna;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "iAjetutKilometrit", {
        get: function () {
            return this._iAjetutKilometrit;
        },
        // iAjetutKilometrit
        set: function (uusi_iAjetutKilometrit) {
            this._iAjetutKilometrit = uusi_iAjetutKilometrit;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "sMerkki", {
        get: function () {
            return this._sMerkki;
        },
        // sMerkki
        set: function (uusi_sMerkki) {
            this._sMerkki = uusi_sMerkki;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "iValmistusvuosi", {
        get: function () {
            return this._iValmistusvuosi;
        },
        // iValmistusvuosi
        set: function (uusi_iValmistusvuosi) {
            this._iValmistusvuosi = uusi_iValmistusvuosi;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CAjoneuvo.prototype, "dMoottorinIskutilavuusLtr", {
        get: function () {
            return this._dMoottorinIskutilavuusLtr;
        },
        // dMoottorinIskutilavuusLtr
        set: function (uusi_dMoottorinIskutilavuusLtr) {
            this._dMoottorinIskutilavuusLtr = uusi_dMoottorinIskutilavuusLtr;
        },
        enumerable: true,
        configurable: true
    });
    return CAjoneuvo;
}());
// ******************************
// *   End of class CAjoneuvo   *
// ******************************
/*
Tehtävä 5.2
Tee ohjelma, joka käyttää hyväkseen edellisen harjoituksen CAjoneuvo-luokkaa.
Ohjelmassa tulee luoda ainakin kaksi uutta auto-oliota ja antaa olioiden ominaisuuksille
arvot. Testaa, että kaikki metodisi toimivat oikein eli palauttavat oikeita arvoja ja antavat
ominaisuuksille oikein oikeat arvot.
*/
// Ohjelma, jolla testataan ajoneuvoja
// Auto 1
var auto1 = new CAjoneuvo("");
auto1.iIskutilavuus = 1;
auto1.iHinta = 2300;
auto1.iOvienLkm = 4;
auto1.bKattoikkuna = false;
auto1.iAjetutKilometrit = 83000;
auto1.sMerkki = "Suzuki";
auto1.iValmistusvuosi = 2004;
auto1.dMoottorinIskutilavuusLtr = 900;
console.log("Auto1: Merkki:" + auto1.sMerkki + ", iskutilavuus:" + auto1.iIskutilavuus
    + ", hinta: " + auto1.iHinta);
// MIKSEI AO: TOIMI ?????
//console.log("Auto1: ovien lkm: " + auto1.iOvienLkm + ", kattoikkuna:" + auto1.bKattoikkuna);
console.log("Auto1: ajetut km: " + auto1.iAjetutKilometrit + ", valm.vuosi: " + auto1.iValmistusvuosi
    + ", moottorin til LTR: " + auto1.dMoottorinIskutilavuusLtr);
console.log("Auto1 kaikki tiedot tulostettu ok.");
// Auto2
var auto2 = new CAjoneuvo("");
auto2.iIskutilavuus = 2;
auto2.iHinta = 23000;
auto2.iOvienLkm = 2;
auto2.bKattoikkuna = true;
auto2.iAjetutKilometrit = 28000;
auto2.sMerkki = "Ford";
auto2.iValmistusvuosi = 2016;
auto2.dMoottorinIskutilavuusLtr = 2000;
console.log("Auto2: Merkki:" + auto2.sMerkki + ", iskutilavuus:" + auto2.iIskutilavuus
    + ", hinta: " + auto2.iHinta);
// MIKSEI AO: TOIMI ?????
//console.log("Auto1: ovien lkm: " + auto1.iOvienLkm + ", kattoikkuna:" + auto1.bKattoikkuna);
console.log("Auto2: ajetut km: " + auto2.iAjetutKilometrit + ", valm.vuosi: " + auto2.iValmistusvuosi
    + ", moottorin til LTR: " + auto2.dMoottorinIskutilavuusLtr);
console.log("Auto2 kaikki tiedot tulostettu ok.");
/*
Tehtävä 5.5

© Ideal Learning Ltd 2018, All Rights Reserved
Onko CAjoneuvo-luokan ominaisuuksia, kuten bKattoluukku, mahdollista määritellä niin,
että se ei ole käytettävissä perivissä luokissa? Missä JavaScript (ECMAScript) versioissa
se on tai ei ole mahdollista? Onko se mahdollista TypeScriptissä tai Java-kielessä?

VASTAUS:
Voidaan tehdä myös private-metodeja tai propertyjä -> tällöin se käytössä vain tässä luokassa, eikä
ole käytettävissä aliluokissa.
- JavaScript -> ei voi käyttää private-määrettä
- TypeScript -> voi käyttää private-määrettä
- Jave -> varmaankin voi käyttää private-määrettä


*/
//**************************************** */
//export default CAjoneuvo;
//# sourceMappingURL=CAjoneuvo.js.map