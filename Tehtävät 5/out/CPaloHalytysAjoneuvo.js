/*
Tehtävä 5.4
Periytä CHalytysAjoneuvo luokasta uusi luokka CPaloHalytysAjoneuvo. Lisää uuteen
luokkaan ainakin paloautoille tutut ominaisuudet, jotka kuvaavat auton tikkaiden korkeutta
ja autossa olevien sammutusletkuliittimien lukumäärää. Tee ominaisuuksille get ja set -
metodit. Testaa myös uuden luokkasi toiminnot ja varmista, että luokkasi toimii.
*/
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// ******************************************
// * Start of class CPaloHalytysAjoneuvo    *
// ******************************************
var CPaloHalytysAjoneuvo = /** @class */ (function (_super) {
    __extends(CPaloHalytysAjoneuvo, _super);
    function CPaloHalytysAjoneuvo() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(CPaloHalytysAjoneuvo.prototype, "iTikasKorkeus", {
        get: function () {
            return this._iTikasKorkeus;
        },
        // ******************************************
        // *    constructor                         *
        // ******************************************
        // ------------------------
        // *******************************************
        // ***** methods: set and get properties *****
        // *******************************************
        // iTikasKorkeus
        set: function (uusi_iTikasKorkeus) {
            this._iTikasKorkeus = uusi_iTikasKorkeus;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CPaloHalytysAjoneuvo.prototype, "iSammutusLetkuLiittimienLkm", {
        get: function () {
            return this._iSammutusLetkuLiittimienLkm;
        },
        // iSammutusLetkuLiittimienLkm
        set: function (uusi_iSammutusLetkuLiittimienLkm) {
            this._iSammutusLetkuLiittimienLkm = uusi_iSammutusLetkuLiittimienLkm;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CPaloHalytysAjoneuvo.prototype, "iVesiSailionTilavuusLtr", {
        get: function () {
            return this._iVesiSailionTilavuusLtr;
        },
        /*
        Tehtävä 5.6 / OK
        Tee palohälytysajoneuvolle uusi ominaisuus, joka kuvaa vesisäiliön tilavuutta (OK). Lisää myös
        uusi metodi, jonka avulla voi selvittää, onko vesisäiliössä vettä vai ei (OK).
        Tee myös metodi, joka kuvaa tilannetta, jossa vesisäiliöstä ruiskutetaan x litraa vettä pois (OK).
        Tee myös metodit, joiden avulla pois ruiskutettavan veden määrä voidaan antaa myös
        Englannin ja Amerikan gallonissa (OK). 1 litra on 0.22 Englannin gallonaa ja 0.26 Amerikan
        gallonaa.
        Kun vesisäiliö on tyhjä, tulee vesisäiliön tilaa(vettä on/ei ole) kuvaavan ominaisuuden
        arvoa muuttaa (OK). Lisää vielä palohälytysajoneuvolle metodi, jolla voidaan selvittää onko
        vesisäiliössä vettä vai ei (OK).
        */
        // iVesiSailionTilavuusLtr
        set: function (iMaxMaara) {
            this._iVesiSailionTilavuusLtr = iMaxMaara;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CPaloHalytysAjoneuvo.prototype, "iVedenMaaraJaljellaLtr", {
        get: function () {
            return this._iVedenMaaraJaljellaLtr;
        },
        // iVedenMaaraJaljellaLtr
        set: function (uusiVedenMaara) {
            this._iVedenMaaraJaljellaLtr = uusiVedenMaara;
        },
        enumerable: true,
        configurable: true
    });
    // metodi: vähennä vettä säiliöstä ltr
    CPaloHalytysAjoneuvo.prototype.vahennaVettaLtr = function (vahennaVettaLtr) {
        if (this.iVedenMaaraJaljellaLtr - vahennaVettaLtr <= 0) {
            this.iVedenMaaraJaljellaLtr = 0;
        }
        else {
            this.iVedenMaaraJaljellaLtr -= vahennaVettaLtr;
        }
    };
    // metodi: onko vesisäiliössä vettä vai ei (palauttaa True/False)
    CPaloHalytysAjoneuvo.prototype.bOnkoVetta = function () {
        if (this.iVedenMaaraJaljellaLtr > 0) {
            return true;
        }
        else {
            return false;
        }
    };
    // metodi: vähennä vettä säiliöstä englannin gallonat
    CPaloHalytysAjoneuvo.prototype.vahennaVettaENGGallons = function (vahennaVettaENGGallons) {
        // 1 litra on 0.22 Englannin gallonaa
        var maaraLitroissa = vahennaVettaENGGallons / 0.22;
        if (this.iVedenMaaraJaljellaLtr - maaraLitroissa <= 0) {
            this.iVedenMaaraJaljellaLtr = 0;
        }
        else {
            this.iVedenMaaraJaljellaLtr -= maaraLitroissa;
        }
    };
    // metodi: vähennä vettä säiliöstä amerikan gallonat
    CPaloHalytysAjoneuvo.prototype.vahennaVettaUSGallons = function (vahennaVettaUSGallons) {
        // 1 litra on 0.26 Amerikan gallonaa
        var maaraLitroissa = vahennaVettaUSGallons / 0.26;
        if (this.iVedenMaaraJaljellaLtr - maaraLitroissa <= 0) {
            this.iVedenMaaraJaljellaLtr = 0;
        }
        else {
            this.iVedenMaaraJaljellaLtr -= maaraLitroissa;
        }
    };
    /*
    Tehtävä 5.7 / OK
    Lisää palohälytysajoneuvolle metodi, jonka avulla vesisäiliötä voidaan täyttää. Vesisäiliö ei
    kuitenkaan saa täyttyä yli äyräidensä; vettä voi olla enintään vesisäiliön tilavuuden verran.
    */
    // metodi: lisää vettä säiliöön ltr
    CPaloHalytysAjoneuvo.prototype.lisaaVettaLtr = function (lisaaVettaLtr) {
        if (this.iVedenMaaraJaljellaLtr + lisaaVettaLtr > this.iVesiSailionTilavuusLtr) {
            this.iVedenMaaraJaljellaLtr = this.iVesiSailionTilavuusLtr;
        }
        else {
            this.iVedenMaaraJaljellaLtr += lisaaVettaLtr;
        }
    };
    return CPaloHalytysAjoneuvo;
}(CHalytysAjoneuvo));
// *****************************************
// *   End of class CPaloHalytysAjoneuvo   *
// *****************************************
// TÄSTÄ KYSYTTÄVÄ!!!!!
// Ohjelma, jolla testataan PaloHalytysAjoneuvo-luokkaa
// 
var paloAuto = new CPaloHalytysAjoneuvo("Volvo");
paloAuto.iIskutilavuus = 5;
paloAuto.iHinta = 250000;
paloAuto.iOvienLkm = 6;
paloAuto.bKattoikkuna = false;
paloAuto.iAjetutKilometrit = 12000;
paloAuto.sMerkki = "Volvo";
paloAuto.iValmistusvuosi = 2018;
paloAuto.dMoottorinIskutilavuusLtr = 5000;
console.log("paloAuto: Merkki:" + paloAuto.sMerkki + ", iskutilavuus:" + paloAuto.iIskutilavuus
    + ", hinta: " + paloAuto.iHinta);
// MIKSEI AO: TOIMI ?????
//console.log("Auto1: ovien lkm: " + auto1.iOvienLkm + ", kattoikkuna:" + auto1.bKattoikkuna);
console.log("paloAuto: ajetut km: " + paloAuto.iAjetutKilometrit + ", valm.vuosi: " + paloAuto.iValmistusvuosi
    + ", moottorin til LTR: " + paloAuto.dMoottorinIskutilavuusLtr);
console.log("paloAuto kaikki tiedot tulostettu ok.");
//*********************************** */
//export default CPaloHalytysAjoneuvo;
//# sourceMappingURL=CPaloHalytysAjoneuvo.js.map