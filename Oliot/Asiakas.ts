//import {database}
var database; // tää pois kun import toimii

// ASIAKAS-luokka (kantaluokka)
// typescript -toteutus luokkana
// abstract class: luokan instanssi voidaan luoda vain alaluokassa (ks. PerusKantaAsiakas.ts)

// ?????????????????? miten?
export abstract class Asiakas {

    protected _etunimi: string;
    protected _sukunimi: string;
    protected bonuskertymä: number;
    protected _asiakasnumero: string;

    // abstakti metodi: määritettävissä vain alaluokassa.
    // metodin toiminto on pakko määrittää alaluokassa, kun abstract-määre.
    // public-määre -> voidaan käyttää esim. softa.js -tiedostossa.
    public abstract laskeBonus(x : Number): void;

    // staattinen metodi: tämä funktio on siis olemassa aina tässä luokassa riippumatta,
    // onko luokan instanssi olemassa. Ts. tällä funktiolla voidaan luoda uusi instanssi
    // luokasta Asiakas hakemalla se tietokannasta.
    static load(id: string) {
        //return new Asiakas(database.lataa(id))
    }

    save() {
      // database.tallenna(this._etunimi, _sukunimi)
    }

    // voidaan tehdä myös private-metodeja -> tällöin käytössä vain tässä luokassa, eikä
    // ole käytettävissä aliluokissa
    
    public setSukunimi (sukunimi: string) { this._sukunimi=sukunimi; }
    public getSukunimi () { return this._sukunimi; }

    // voi tehdä näin myös typescriptin set:llä (ks. dokumentaatio)
    set etunimi(uusiEtunimi: string) {
        this._etunimi = uusiEtunimi;
    }
    // voi tehdä näin myös typescriptin get:llä (ks. dokumentaatio)
    get etunimi(): string {
        return this._etunimi;
    }

    // konstruktori
    constructor(etunimi: string, sukunimi: string = "") { 
        this._etunimi = etunimi;
        this._sukunimi = sukunimi;
    }

}

//const joku = new Asiakas("timo metsälä");
//joku.save();
//console.log(joku);