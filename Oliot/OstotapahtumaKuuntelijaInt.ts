// interface -kuuntelija ostotapahtumalle: seuraa Ostotapahtuma -olion tilaa
// hyvä tapa nimeykseen: loppuun 'Int'

import {Ostotapahtuma} from "./Ostotapahtuma";

export interface OstotapahtumaKuuntelijaInt {

    tapahtui(lähde: Ostotapahtuma): void;

}