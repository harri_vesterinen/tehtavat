// PREMIUMKANTAASIAKAS-aliluokka
// typescript -toteutus luokkana
// alaluokka: perii ylemmältä luokalta Asiakas (ks. Asiakas.ts)

import { Asiakas } from "./Asiakas";

class PremiumKantaAsiakas extends Asiakas {

    laskeBonus(x:number){
        this.bonuskertymä = x * 4;
    };

}

//let a = new PremiumKantaAsiakas("Pekka");
//console.log(a);