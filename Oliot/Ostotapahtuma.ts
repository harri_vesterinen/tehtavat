import { Asiakas } from "./Asiakas";
import { OstotapahtumaKuuntelijaInt } from "./OstotapahtumaKuuntelijaInt"
import { OstotapahtumaKuunneltavaInt } from "./OstotapahtumaKuunneltavaInt";
import { Tuote } from "./Tuote";

// enum: luetteloarvot
export enum Tila {kesken,luotu,maksettu};

export class Ostotapahtuma implements OstotapahtumaKuunneltavaInt {

    private kuuntelijat: OstotapahtumaKuuntelijaInt[]; // mitkä kaikki kuuntelevat Ostotapahtuman tilan muutoksia.
    private tapahtumanTila: Tila.kesken;
    private määrä: number;
    private tuote: Tuote;
    private asiakas: Asiakas;
    private ostohetki: string;

    constructor(asiakas: Asiakas, tuote: Tuote, määrä: number) {
        this.määrä = määrä;
        this.asiakas = asiakas;
        this.tuote = tuote;
        this.kuuntelijat = [];
    }

    // kuuntelijan lisääminen kuuntelijat-taulukkoon -> tartteeko tätä enää ks. alle
    public lisääOstotapahtumaKuuntelija(kuuntelija: OstotapahtumaKuuntelijaInt) 
    {
        // Onkohan siellä kuuntelijassa mitään? FIXME!
        this.kuuntelijat.push(kuuntelija);
    }

    //???????
    public maksa() {
        this.$tapahtumanTila = Tila.maksettu;
    }

    //
    public lisääKuuntelija(kuuntelija: OstotapahtumaKuuntelijaInt)
    {
        // Onkohan siellä kuuntelijassa mitään? FIXME!
        this.kuuntelijat.push(kuuntelija);
    }
    //
    public poistaKuuntelija(kuuntelija: OstotapahtumaKuuntelijaInt)
    {
        // Onkohan siellä kuuntelijassa mitään? FIXME!
    }
    //
    public ilmianna() {
        this.kuuntelijat.forEach(kuuntelija=>{
            kuuntelija.tapahtui(this);
        })
    }
  
    public set $asiakas(value: Asiakas) {
        this.asiakas = value;
    }

    public set $tapahtumanTila(value: Tila) {
        if (value != this.tapahtumanTila) {
            this.tapahtumanTila = value;
            this.ilmianna();
        }
    }

}