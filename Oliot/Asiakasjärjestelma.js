// ASIAKASJÄRJESTELMÄN SUUNNITTELU

// (1) ensin suunnitellaan tarvittavat toiminnot eli funktiot karkealla tasolla yhteen tiedostoon
// (2) sitten pitää miettiä modulointia eli koodin jakamista eri tiedostoiksi järkevästi
// - esim. toisiinsa liittyvät funktiot yhteen js-tiedostoon

// kommunikaatiokanava toiseen järjestelmään, ikäänkuin viestijono:
// sinne tulleet "viestit" käsitellään jonossa joko FIFO-periaatteella tai jollain priorisoidulla systeemillä
MessageChannel.eventHandler(kerrytäBonuksia) // tässä MessageChannel on vain esimerkki jostain viestikanavasta
// kerrytäBonuksia-funktio määritetty alempana

//UlkoinenJärjestelmä ulkoinen; // jokin ukopuolinen järjestelmä, joka haluaa tietoa tästä systeemistä
ulkoinen = ["J1","J2","J3"] // tässä 3 kpl jotain ulkoisia systeemejä
database = DB.Open // avataan joku tietokanta

function ilmoitaUlkopuoliselleJärjestelmälle() {
    ulkoinen.forEach( (item) => {
        item.lisääAsiakas()} )
}

function lisääAsiakas(nimi, sotu) {
    // jotain tämän järjestelmän juttuja

    // ilmoitus ulkopuoliselle systeemille
    ilmoitaUlkopuoliselleJärjestelmälle()
}
function poistaAsiakas(sotu) {

}
// obj {etunimi:"Lasse"}
function päivitäAsiakkaantiedot(asiakasnumero, obj) {
    // esim. Mongo-kantaan päivitetään kaikki obj:ssa tuleva asiakasdata
    // tämä pätee ns. dokumenttitietokantoihin, kuten Mongo, joihin voi vaikka lisätä uusia kenttiä
    // relaatiokannassa pitää olla olemassa kaikki ne kentät, jotka obj välittää.
    päivitäKanta(Mongo.Päivitä(obj, asiakasnumero))

    // näin relaatiokantaan
    päivitäKanta(database, asiakasnumero, obj)
}

// REST-funktio
function haeAsiakas(asiakasnumero) {
    a = haeAsiakas(database, asiakasnumero);
    return a;
}

// Viestin lähettäminen: milloin toiminto käynnistyy?
// Message queye -> event handler -> callback (ks. alussa MessageChannel-kohta)
function lähetäViesti(asiakasnumero, viesti, väline) {
    switch (väline) {
        case 0:
            lähetäSMS(asiakasnumero, viesti)
            break
        case 1:
            lähetäSähköposti(asiakasnumero, viesti)
            break
        case 2:
            lähetäKirjeposti(asiakasnumero, viesti)
            break
        default:
            lähetäMorsekoodilla(asiakasnumero, viesti)
    }
}

function lähetäSMS(asiakasnumero, viesti) {
    SMSS.Send(puhno, viesti) // SMSS on jokin erikseen kodattu kirjasto (3. osapuolen)
}
function lähetäSähköposti(asiakasnumero, viesti) {
    MailClient.Send(sähköposti, viesti) // MailClient on jokin erikseen kodattu kirjasto (3. osapuolen)
}
function lähetäKirjeposti(asiakasnumero, viesti) {
    SuomenPosti.Send(osoite, viesti) // SuomenPosti on jokin erikseen kodattu kirjasto (3. osapuolen)
}
function lähetäMorsekoodilla(asiakasnumero, viesti) {

}

// Bonusten kerryttäminen: milloin toiminto käynnistyy?
// Message queye -> event handler -> callback (ks. alussa MessageChannel-kohta)
function kerrytäBonuksia(asiakasnumero, lisättäväBonus) {

}

