var Tuote = /** @class */ (function () {
    function Tuote() {
    }
    Object.defineProperty(Tuote.prototype, "$nimi", {
        get: function () {
            return this.nimi;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Tuote.prototype, "$hinta", {
        get: function () {
            return this.hinta;
        },
        enumerable: true,
        configurable: true
    });
    return Tuote;
}());
//# sourceMappingURL=Tuote.js.map