"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//import {database}
var database; // tää pois kun import toimii
// ASIAKAS-luokka (kantaluokka)
// typescript -toteutus luokkana
// abstract class: luokan instanssi voidaan luoda vain alaluokassa (ks. PerusKantaAsiakas.ts)
// ?????????????????? miten?
var Asiakas = /** @class */ (function () {
    // konstruktori
    function Asiakas(etunimi, sukunimi) {
        if (sukunimi === void 0) { sukunimi = ""; }
        this._etunimi = etunimi;
        this._sukunimi = sukunimi;
    }
    // staattinen metodi: tämä funktio on siis olemassa aina tässä luokassa riippumatta,
    // onko luokan instanssi olemassa. Ts. tällä funktiolla voidaan luoda uusi instanssi
    // luokasta Asiakas hakemalla se tietokannasta.
    Asiakas.load = function (id) {
        //return new Asiakas(database.lataa(id))
    };
    Asiakas.prototype.save = function () {
        // database.tallenna(this._etunimi, _sukunimi)
    };
    // voidaan tehdä myös private-metodeja -> tällöin käytössä vain tässä luokassa, eikä
    // ole käytettävissä aliluokissa
    Asiakas.prototype.setSukunimi = function (sukunimi) { this._sukunimi = sukunimi; };
    Asiakas.prototype.getSukunimi = function () { return this._sukunimi; };
    Object.defineProperty(Asiakas.prototype, "etunimi", {
        // voi tehdä näin myös typescriptin get:llä (ks. dokumentaatio)
        get: function () {
            return this._etunimi;
        },
        // voi tehdä näin myös typescriptin set:llä (ks. dokumentaatio)
        set: function (uusiEtunimi) {
            this._etunimi = uusiEtunimi;
        },
        enumerable: true,
        configurable: true
    });
    return Asiakas;
}());
exports.Asiakas = Asiakas;
//const joku = new Asiakas("timo metsälä");
//joku.save();
//console.log(joku);
//# sourceMappingURL=Asiakas.js.map