"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// enum: luetteloarvot
var Tila;
(function (Tila) {
    Tila[Tila["kesken"] = 0] = "kesken";
    Tila[Tila["luotu"] = 1] = "luotu";
    Tila[Tila["maksettu"] = 2] = "maksettu";
})(Tila || (Tila = {}));
;
var Ostotapahtuma = /** @class */ (function () {
    function Ostotapahtuma() {
    }
    // kuuntelijan lisääminen kuuntelijat-taulukkoon
    Ostotapahtuma.prototype.lisääOstotapahtumaKuuntelija = function (kuuntelija) {
        // Onkohan siellä kuuntelijassa mitään? FIXME!
        this.kuuntelijat.push(kuuntelija);
    };
    Object.defineProperty(Ostotapahtuma.prototype, "$asiakas", {
        set: function (value) {
            this.asiakas = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Ostotapahtuma.prototype, "$tapahtumanTila", {
        set: function (value) {
            this.tapahtumanTila = value;
            // interface
            //objekti.tapahtui();
        },
        enumerable: true,
        configurable: true
    });
    return Ostotapahtuma;
}());
exports.Ostotapahtuma = Ostotapahtuma;
//# sourceMappingURL=Ostotapahtuma.js.map