"use strict";
// PERUSKANTAASIAKAS-aliluokka
// typescript -toteutus luokkana
// alaluokka: perii ylemmältä luokalta Asiakas (ks. Asiakas.ts)
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Asiakas_1 = require("./Asiakas");
var PerusKantaAsiakas = /** @class */ (function (_super) {
    __extends(PerusKantaAsiakas, _super);
    function PerusKantaAsiakas() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PerusKantaAsiakas.prototype.laskeBonus = function (x) {
        this.bonuskertymä = x * 2;
    };
    ;
    return PerusKantaAsiakas;
}(Asiakas_1.Asiakas));
//let a = new PerusKantaAsiakas("Pekka");
//console.log(a);
//# sourceMappingURL=PerusKantaAsiakas.js.map