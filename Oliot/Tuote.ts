export class Tuote {

    private nimi: string;
    private hinta: number;

    constructor(nimi: string, hinta: number) {
        this.nimi = nimi;
        this.hinta = hinta;
    }

    public get $nimi(): string {
        return this.nimi;
    }

    public get $hinta(): number {
        return this.hinta;
    }
    
}