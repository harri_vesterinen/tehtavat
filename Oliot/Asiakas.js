// JavaScriptissä käsitteenä on "objekti" (ei ole teknisesti "olio").
// JavaScriptissa voidaan ajon aikana muuttaa/lisätä/poistaa mitä tahansa objektien propertyjä.
// MYös esim. __proto__ -arvoa, eli mistä luokka peritään.
// Siinä mielessä JS ei ole perinteinen oliokieli. Ei siis ole turvallinen oliokieli.
// JS: "dynaaminen oliomalli", turvaton, "kädet ristiin" -juttu.

// on hyvä siinä mielessä, että voidaan hakea serveriltä JSON-dataa vaikka
// useista eri paikoista (tauluista) ja yhdistellä niitä koodissa esim. objektiin, jonka sisällä
// voi olla toisia objekteja -> näppärä tapa käyttää sitten UI:ssa

// esimerkki Asiakas-objekti, Kanta-asiakasobjekti
// prototyyppiobjekti "Asiakas": määritetään yleensä vain funktiot, joilla esim. kenttiä muutetaan
Asiakas = {
    sNimi : "Liikanen", // voisi olla toki myös tietokenttiä
    setNimi : (p) => { this.sNimi = p }, // esim. metodi, jolla muutetaan asiakkaan sNimi:ä
    // fn1() muut funktiot
    // fn2()
    // fn3()
    __proto__ //kaikissa olioissa on prototyyppikenttä // tämä liittyy perintään
}

KantaAsiakas = {
    iBonusKertymä : 20,
    __proto__ = Asiakas, //kaikissa objekteissa on prototyyppikenttä // tämä liittyy perintään
    lapset : { } // voi olla objekti objektin sisällä
}

KantaAsiakas.prototype = Asiakas // esim. objektin kantaobjektin asettaminen
KantaAsiakas.setNimi("Järvinen") // esim. nimen asettaminen
KantaAsiakas.iBonusKertymä = 50

delete KantaAsiakas.iBonusKertymä // näin voi poistaa oliolta propertyn


