// PERUSKANTAASIAKAS-aliluokka
// typescript -toteutus luokkana
// alaluokka: perii ylemmältä luokalta Asiakas (ks. Asiakas.ts)

import { Asiakas } from "./Asiakas";

export class PerusKantaAsiakas extends Asiakas {

    laskeBonus(x:number){
        this.bonuskertymä = x * 2;
    };

}

//let a = new PerusKantaAsiakas("Pekka");
//console.log(a);